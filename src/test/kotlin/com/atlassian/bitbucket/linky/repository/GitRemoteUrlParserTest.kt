package com.atlassian.bitbucket.linky.repository

import assertk.all
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import assertk.assertions.prop
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.GIT
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.atlassian.bitbucket.linky.UriScheme.HTTPS
import com.atlassian.bitbucket.linky.UriScheme.SSH
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import org.junit.jupiter.api.Test

internal class GitRemoteUrlParserTest {
    @Test
    fun `test parse Cloud HTTP remote URL`() {
        assertUrlParsed(
            "https://user@staging.bb-inf.net/bitbucket/test.git",
            HTTPS,
            "staging.bb-inf.net",
            -1,
            "/bitbucket/test"
        )
        assertUrlParsed(
            "http://user@bitbucket.org/atlas/some-project.git",
            HTTP,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "https://user@bitbucket.org/atlas/some-project.git",
            HTTPS,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
    }

    @Test
    fun `test parse Cloud SSH remote URL`() {
        assertUrlParsed(
            "git-staging@bitbucket.org:atlas/some-project",
            SSH,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git@bitbucket.org:atlas/some-project.git",
            SSH,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@bitbucket.org:atlas/some-project.git",
            SSH,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@bitbucket.org:atlas/some-project",
            SSH,
            "bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git-staging@staging.bitbucket.org:atlas/some-project",
            SSH,
            "staging.bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git@staging.bitbucket.org:atlas/some-project",
            SSH,
            "staging.bitbucket.org",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git-staging@staging.bb-inf.net:bitbucket/test.git",
            SSH,
            "staging.bb-inf.net",
            -1,
            "/bitbucket/test"
        )
        assertUrlParsed(
            "git@bitbucket.org:1234/5678",
            SSH,
            "bitbucket.org",
            -1,
            "/1234/5678",
        )
        assertUrlParsed(
            "git@bitbucket.org:/5678/1234",
            SSH,
            "bitbucket.org",
            -1,
            "/5678/1234",
        )
    }

    @Test
    fun `test parse server HTTP remote URL`() {
        assertUrlParsed(
            "http://user@some-host.com/atlas/some-project.git",
            HTTP,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "http://user@some-host.com/atlas/some-project",
            HTTP,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "http://git@some-host.com/some/path/atlas/some-project.git",
            HTTP,
            "some-host.com",
            -1,
            "/some/path/atlas/some-project"
        )
        assertUrlParsed(
            "http://git@some-host.com:7999/some/path/atlas/some-project.git",
            HTTP,
            "some-host.com",
            7999,
            "/some/path/atlas/some-project"
        )
        assertUrlParsed(
            "https://git@some-host.com/atlas/some-project.git",
            HTTPS,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "https://some-host.com/some/path/",
            HTTPS,
            "some-host.com",
            -1,
            "/some/path"
        )
    }

    @Test
    fun `test parse server SSH remote URL`() {
        assertUrlParsed(
            "git://some-host.com/atlas/some-project.git",
            GIT,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@some-host.com/atlas/some-project.git",
            SSH,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git@some-host.com/atlas/some-project.git",
            SSH,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git@some-host.com/atlas/some-project.git",
            SSH,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "git@some-host.com/atlas/some-project.git",
            SSH,
            "some-host.com",
            -1,
            "/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@some-host.com/some/path/atlas/some-project.git",
            SSH,
            "some-host.com",
            -1,
            "/some/path/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@some-host.com:1234/some/path/atlas/some-project.git",
            SSH,
            "some-host.com",
            1234,
            "/some/path/atlas/some-project"
        )
        assertUrlParsed(
            "ssh://git@some-host.com:7999/some/path/~dpenkin/test-repository.git",
            SSH,
            "some-host.com",
            7999,
            "/some/path/~dpenkin/test-repository"
        )
        assertUrlParsed(
            "ssh://git@some-host.com:1234/some/path/atlas/some-project.git",
            SSH,
            "some-host.com",
            1234,
            "/some/path/atlas/some-project"
        )
    }

    @Test
    fun `test parse other remote URLs`() {
        assertUrlParsed(
            "example",
            SSH,
            "example",
            -1,
            "/"
        )
        assertUrlParsed(
            "http://example",
            HTTP,
            "example",
            -1,
            "/"
        )
        assertUrlParsed(
            "http://example.com",
            HTTP,
            "example.com",
            -1,
            "/"
        )
        assertUrlParsed(
            "http://example.com/",
            HTTP,
            "example.com",
            -1,
            "/"
        )
        assertUrlParsed(
            "http://example.com:239",
            HTTP,
            "example.com",
            239,
            "/"
        )
        assertUrlParsed(
            "http://example.com:239/",
            HTTP,
            "example.com",
            239,
            "/"
        )
        assertUrlParsed(
            "http://example.com:239/hello/world/",
            HTTP,
            "example.com",
            239,
            "/hello/world"
        )
    }

    @Test
    fun `test parse other schemes returns null`() {
        assertThat(GitRemoteUrlParser.parseRemoteUrl("file://whatever")).isNull()
    }

    private fun assertUrlParsed(
        urlToParse: String,
        expectedScheme: UriScheme,
        expectedHostname: String,
        expectedPort: Int,
        expectedPath: String
    ) {
        val url = GitRemoteUrlParser.parseRemoteUrl(urlToParse)
        assertThat(url).isNotNull().all {
            prop(RemoteUrl::scheme).isEqualTo(expectedScheme)
            prop(RemoteUrl::hostname).isEqualTo(expectedHostname)
            prop(RemoteUrl::port).isEqualTo(expectedPort)
            prop(RemoteUrl::path).isEqualTo(expectedPath)
        }
    }
}
