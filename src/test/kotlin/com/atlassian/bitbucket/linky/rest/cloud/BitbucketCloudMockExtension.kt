package com.atlassian.bitbucket.linky.rest.cloud

import com.github.tomakehurst.wiremock.WireMockServer
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver
import kotlin.random.Random

class BitbucketCloudMockExtension : BeforeAllCallback, ParameterResolver, AfterEachCallback, AfterAllCallback {
    override fun beforeAll(context: ExtensionContext?) {
        ServerHolder.bitbucketCloudMock.start()
        System.setProperty(BBC_MOCK_URL, ServerHolder.bitbucketCloudMock.baseUrl())
    }

    override fun supportsParameter(
        parameterContext: ParameterContext,
        extensionContext: ExtensionContext,
    ): Boolean = parameterContext.parameter.type == WireMockServer::class.java

    override fun resolveParameter(
        parameterContext: ParameterContext,
        extensionContext: ExtensionContext,
    ): WireMockServer = ServerHolder.bitbucketCloudMock

    override fun afterEach(context: ExtensionContext?) {
        ServerHolder.bitbucketCloudMock.resetRequests()
        ServerHolder.bitbucketCloudMock.resetToDefaultMappings()
    }

    override fun afterAll(context: ExtensionContext?) {
        ServerHolder.bitbucketCloudMock.stop()
        System.clearProperty(BBC_MOCK_URL)
    }
}

// Start mock server at random port, but since BitbucketCloud is a singleton,
// the port needs to be selected once for the entire test session. This is why
// the server is created in another singleton which is instantiated only once.
private object ServerHolder {
    val bitbucketCloudMock = WireMockServer(Random.nextInt(5000, 10000))
}
