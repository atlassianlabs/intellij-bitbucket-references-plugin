package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.blame.LineBlamer
import com.atlassian.bitbucket.linky.preferences.Preferences
import com.atlassian.bitbucket.linky.preferences.preferences
import com.intellij.dvcs.repo.Repository
import com.intellij.mock.MockVirtualFile
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.jupiter.api.BeforeEach

abstract class AbstractLinkyTest {

    protected val lineBlamer = mockk<LineBlamer>()
    protected val repository = mockk<Repository>()
    protected val preferences = mockk<Preferences>()

    @BeforeEach
    open fun setUp() {
        mockkStatic("com.atlassian.bitbucket.linky.preferences.RepositoryPreferencesKt")
        every { repository.preferences() } returns preferences
        every { preferences.getProperty(any(), any()) } returns null
    }

    protected fun linkyFile(relativePath: String = "dir", filename: String = "file"): LinkyFile {
        val virtualFile = MockVirtualFile(filename)
        val linkyFile = LinkyFile(virtualFile, repository, "someRevision", "$relativePath/$filename", lineBlamer)

        every { lineBlamer.blameLine(linkyFile, 239) } returns null

        return linkyFile
    }
}
