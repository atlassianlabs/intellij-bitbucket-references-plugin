package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.configuration.UpdateChannel.STABLE
import com.intellij.openapi.components.service
import com.intellij.openapi.updateSettings.impl.UpdateSettings
import com.intellij.ui.dsl.builder.MutableProperty

enum class UpdateChannel {
    STABLE, EAP, NIGHTLY;

    val url = "https://plugins.jetbrains.com/plugins/${name.lowercase()}/8015"

    fun isInHosts() = when (this) {
        STABLE -> false
        else -> url in service<UpdateSettings>().storedPluginHosts
    }
}

object UpdateChannelProperty : MutableProperty<UpdateChannel?> {

    override fun get(): UpdateChannel = UpdateChannel.values().find { it.isInHosts() } ?: STABLE

    override fun set(value: UpdateChannel?) {
        val ch = value ?: STABLE
        val hosts = UpdateSettings.getInstance().storedPluginHosts
        hosts.removeIf {
            it in UpdateChannel.values().map { c -> c.url }
        }
        if (ch != STABLE) {
            hosts.add(ch.url)
        }
    }
}
