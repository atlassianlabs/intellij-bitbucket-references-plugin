package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.discovery.matchesCloudPattern
import com.atlassian.bitbucket.linky.discovery.matchesServerPattern
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.atlassian.bitbucket.linky.repository.traverseRepositories
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.SearchableConfigurable
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MasterDetailsComponent
import com.intellij.openapi.ui.MasterDetailsStateService
import com.intellij.util.ui.tree.TreeUtil
import java.awt.Dimension

class RepositoriesConfigurable(
    private val project: Project,
) : MasterDetailsComponent(), SearchableConfigurable, Configurable {
    private val linkedReposParentNode =
        MyNode(RepoGroupConfigurableItem(message("preferences.repositories.tree.node.linked.title")), true)
    private val linkableReposNode =
        MyNode(RepoGroupConfigurableItem(message("preferences.repositories.tree.node.linkable.title")), true)
    private val unsupportedReposNode =
        MyNode(RepoGroupConfigurableItem(message("preferences.repositories.tree.node.unsupported.title")), true)

    init {
        initTree()
    }

    override fun getId(): String = "com.atlassian.bitbucket.linky.repositories"

    override fun getDisplayName(): String = message("preferences.repositories.title")

    override fun initTree() {
        super.initTree()

        val emptyText = myTree.emptyText
        emptyText.text = message("preferences.repositories.tree.empty.text")
    }

    override fun getEmptySelectionString(): String = message("preferences.repositories.tree.empty.selection.text")

    override fun getStateService(): MasterDetailsStateService =
        MasterDetailsStateService.getInstance(project)

    override fun getComponentStateKey(): String = "BitbucketLinky.Repositories.UI"

    override fun reset() {
        reloadRepositories()
        super<MasterDetailsComponent>.reset()

        master.minimumSize = Dimension(200, master.minimumSize.height)
        TreeUtil.expandAll(myTree)
    }

    override fun apply() {
        val selectedRepo = selectedObject
        val oldNodePath = selectedNode?.path

        super.apply()

        project.service<BitbucketRepositoriesService>().configurationChanged()
        reloadRepositories()

        if (selectedRepo != null) {
            val newRepoNode = findNodeByObject(myRoot, selectedRepo)
            if (newRepoNode?.path.contentEquals(oldNodePath).not()) {
                selectNodeInTree(newRepoNode)
            }
        }
    }

    private fun reloadRepositories() {
        val bbRepoService = project.service<BitbucketRepositoriesService>()
        val allRepos = project.traverseRepositories()

        val linkedRepos = allRepos.filter { bbRepoService.getBitbucketRepositories(it).isNotEmpty() }
        val linkableRepos = allRepos
            .filterNot { it in linkedRepos }
            .filter { it.getRemoteUrls().any { url -> url.matchesCloudPattern() || url.matchesServerPattern() } }
        val unsupportedRepos = allRepos
            .filterNot { it in linkedRepos }
            .filterNot { it in linkableRepos }

        linkedReposParentNode.update(linkedRepos) { LinkedRepoConfigurableItem(it) }
        linkableReposNode.update(linkableRepos) { LinkableRepoConfigurableItem(it) }
        unsupportedReposNode.update(unsupportedRepos) { UnsupportedRepoConfigurableItem(it) }
    }

    private fun MyNode.update(
        repositories: List<Repository>,
        createItem: (Repository) -> RepositoryConfigurableItem,
    ) {
        if (repositories.isEmpty()) {
            removeNodes(listOf(this))
        } else {
            // Re-add to the root if hidden
            if (parent == null) {
                addNode(this, myRoot)
            }

            // Remove existing children which are not present in the new values
            val obsolete = children()
                .toList()
                .filterIsInstance<MyNode>()
                .filterNot { childNode ->
                    val repo = (childNode.userObject as? RepositoryConfigurableItem)?.repository
                    repo in repositories
                }
            removeNodes(obsolete)

            val existing = children()
                .toList()
                .filterIsInstance<MyNode>()
                .mapNotNull { it.userObject as? RepositoryConfigurableItem }
                .map { it.repository }

            // Add missing values from the new values list
            (repositories - existing).forEach { addNode(MyNode(createItem(it)), this) }
        }
    }
}
