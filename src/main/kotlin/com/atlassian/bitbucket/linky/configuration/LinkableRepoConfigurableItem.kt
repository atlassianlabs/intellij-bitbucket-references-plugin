package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.discovery.BitbucketCloudProbe
import com.atlassian.bitbucket.linky.discovery.ManualRepositoryLinker
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.discovery.matchesCloudPattern
import com.atlassian.bitbucket.linky.discovery.matchesServerPattern
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.ui.ComboBox
import com.intellij.openapi.ui.DialogPanel
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.util.Disposer
import com.intellij.ui.CollectionComboBoxModel
import com.intellij.ui.components.JBTextField
import com.intellij.ui.dsl.builder.COLUMNS_MEDIUM
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.columns
import com.intellij.ui.dsl.builder.listCellRenderer
import com.intellij.ui.dsl.builder.panel
import com.intellij.ui.dsl.builder.text
import com.intellij.ui.layout.ValidationInfoBuilder
import com.intellij.util.text.nullize
import com.intellij.util.ui.JBEmptyBorder
import com.intellij.util.ui.UIUtil
import java.net.URI
import javax.swing.JComponent

class LinkableRepoConfigurableItem(
    repository: Repository,
) : RepositoryConfigurableItem(repository, BitbucketLinkyIcons.Settings.LinkableRepository) {

    private val panel: DialogPanel by lazy {
        panel {
            row(message("preferences.repositories.tree.node.linkable.path.label")) {
                textField()
                    .columns(COLUMNS_MEDIUM)
                    .text(repository.root.path)
                    .applyToComponent { isEditable = false }
            }

            lateinit var remoteUrlComboBox: Cell<ComboBox<RemoteUrl>>
            row(message("preferences.repositories.tree.node.linkable.remote.url.label")) {
                val remoteUrlOptions = repository.getRemoteUrls()
                    .filter { url -> url.matchesCloudPattern() || url.matchesServerPattern() }

                @Suppress("DEPRECATION")
                remoteUrlComboBox = comboBox(
                    CollectionComboBoxModel(remoteUrlOptions),
                    listCellRenderer { value -> text = value.serialize() },
                )
                    .columns(COLUMNS_MEDIUM)
                    .comment(message("preferences.repositories.tree.node.linkable.remote.url.comment"))
            }

            lateinit var urlTextField: Cell<JBTextField>
            row(message("preferences.repositories.tree.node.linkable.url.label")) {
                urlTextField = textField()
                    .columns(COLUMNS_MEDIUM)
                    .comment(message("preferences.repositories.tree.node.linkable.url.comment"))
                    .onIsModified { urlTextField.component.text.isNotBlank() }
                    .validationOnInput { urlField ->
                        (remoteUrlComboBox.component.selectedItem as? RemoteUrl)
                            ?.let { remoteUrl -> validateRepositoryUrl(remoteUrl, urlField.text) }
                    }
                    .onApply {
                        val rawUrl = urlTextField.component.text.nullize(nullizeSpaces = true) ?: return@onApply
                        val remoteUrl = remoteUrlComboBox.component.selectedItem as? RemoteUrl ?: return@onApply
                        val url = runCatching { URI(rawUrl) }.getOrNull() ?: return@onApply
                        if (url.host == null || url.path == null) {
                            return@onApply
                        }

                        val linker = getManualLinker()
                        when (val cloud = BitbucketCloudProbe.probeBitbucketCloud(url.host)) {
                            null -> getBitbucketServer(url)?.let { server -> linker.register(remoteUrl, server) }
                            else -> linker.register(remoteUrl, cloud)
                        }
                    }
                    .applyToComponent {
                        emptyText.text = message("preferences.repositories.tree.node.linkable.url.empty.text")
                    }
            }

            feedbackFooter()
        }.withBorder(JBEmptyBorder(UIUtil.getRegularPanelInsets()))
    }

    override fun createOptionsPanel(): JComponent = panel
        .also {
            val disposable = Disposer.newDisposable()
            panel.registerValidators(disposable)
            Disposer.register(repository, disposable)
        }

    override fun isModified(): Boolean = panel.isModified()

    override fun apply() {
        panel.apply()
    }

    private fun ValidationInfoBuilder.validateRepositoryUrl(remoteUrl: RemoteUrl, input: String): ValidationInfo? {
        val rawUrl = input.nullize(nullizeSpaces = true) ?: return null
        val url = runCatching { URI(rawUrl) }.getOrNull()

        if (url == null || url.host == null || url.path == null) {
            return error(message("preferences.repositories.tree.node.linkable.url.malformed.text"))
        }

        if (
            // Do not warn until user finishes typing in hostname
            url.path.isNullOrBlank().not() &&
            // Only warn if the target host is not Bitbucket Cloud
            url.host != BitbucketCloud.baseUrl.host &&
            remoteUrl.matchesServerPattern() &&
            getBitbucketServer(url) == null
        ) {
            return warning(message("preferences.repositories.tree.node.linkable.url.unrecognized.text"))
        }

        return null
    }

    private fun getBitbucketServer(url: URI): BitbucketServer? {
        val segments = url.path.split('/')
        // BBS URL path should be .../projects/KEY/repos/SLUG/...
        // This check however deliberately ignores the SLUG and passes
        // as soon as the slash is added after `repos` path segment.
        val projectsIdx = segments.lastIndexOf("projects")
        val reposIdx = segments.lastIndexOf("repos")
        return if (projectsIdx >= 0 && reposIdx == projectsIdx + 2) {
            val bbRootPath = url.path.substring(0, url.path.lastIndexOf("/projects/") + 1)
            BitbucketServer(url.resolve(bbRootPath))
        } else {
            null
        }
    }

    private fun getManualLinker(): ManualRepositoryLinker {
        return service()
    }
}
