package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LINKY_ISSUES_URL
import com.atlassian.bitbucket.linky.LinkyBundle
import com.intellij.ide.BrowserUtil
import com.intellij.ui.dsl.builder.AlignX
import com.intellij.ui.dsl.builder.Panel

fun Panel.feedbackFooter() {
    row { label("") }.resizableRow()
    separator()
    row {
        comment(
            LinkyBundle.message(
                "preferences.feedback.text",
                "<a>${LinkyBundle.message("preferences.feedback.link.text")}</a>"
            )
        ) { BrowserUtil.browse(LINKY_ISSUES_URL) }
            .align(AlignX.RIGHT)
    }
}
