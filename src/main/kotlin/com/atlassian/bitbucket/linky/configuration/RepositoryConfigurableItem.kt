package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.ui.NamedConfigurable
import com.intellij.util.ui.JBUI
import java.awt.BorderLayout
import javax.swing.Icon
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.SwingConstants

abstract class RepositoryConfigurableItem(
    val repository: Repository,
    private val icon: Icon,
) : NamedConfigurable<Repository>() {
    override fun getDisplayName(): String = repository.root.name
    override fun setDisplayName(name: String?) {}
    override fun getEditableObject(): Repository = repository
    override fun getBannerSlogan(): String = message("preferences.repositories.tree.banner")
    override fun getIcon(expanded: Boolean): Icon? = icon
}

class RepoGroupConfigurableItem(private val name: String) : NamedConfigurable<String>() {
    override fun isModified() = false
    override fun apply() {}
    override fun getDisplayName() = name
    override fun setDisplayName(name: String?) {}
    override fun getEditableObject() = name
    override fun getBannerSlogan() = message("preferences.repositories.tree.banner")
    override fun createOptionsPanel() =
        emptyDetailsPanel(message("preferences.repositories.tree.empty.selection.text"))
}

class UnsupportedRepoConfigurableItem(
    repository: Repository,
) : RepositoryConfigurableItem(repository, BitbucketLinkyIcons.Settings.UnknownRepository) {
    override fun isModified() = false
    override fun apply() {}
    override fun createOptionsPanel() =
        emptyDetailsPanel(message("preferences.repositories.tree.node.unsupported.text"))
}

private fun emptyDetailsPanel(message: String) =
    JPanel(BorderLayout()).apply {
        add(
            JLabel(message, SwingConstants.CENTER).apply {
                foreground = JBUI.CurrentTheme.ContextHelp.FOREGROUND
            }
        )
    }
