package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.configuration.RemoteUrlOption.Auto
import com.atlassian.bitbucket.linky.configuration.RemoteUrlOption.Fixed
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.repository.alwaysLinkToSelectedRemote
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.atlassian.bitbucket.linky.repository.pullRequestDefaultTargetBranchName
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.CollectionComboBoxModel
import com.intellij.ui.dsl.builder.COLUMNS_MEDIUM
import com.intellij.ui.dsl.builder.MutableProperty
import com.intellij.ui.dsl.builder.bindItem
import com.intellij.ui.dsl.builder.columns
import com.intellij.ui.dsl.builder.listCellRenderer
import com.intellij.ui.dsl.builder.panel
import com.intellij.ui.layout.selectedValueMatches
import com.intellij.util.ui.JBEmptyBorder
import com.intellij.util.ui.UIUtil
import javax.swing.JComponent
import javax.swing.text.JTextComponent

class LinkedRepoConfigurableItem(
    repository: Repository,
) : RepositoryConfigurableItem(repository, BitbucketLinkyIcons.Bitbucket) {
    private val panel: DialogPanel by lazy {
        panel {
            row(message("preferences.repositories.tree.node.linked.remote.url.label")) {
                val bbRepos = repository.project.service<BitbucketRepositoriesService>()
                    .getBitbucketRepositories(repository)

                val remoteUrlOptions = listOf(Auto) +
                    bbRepos.keys
                        .sortedBy { it.serialize() }
                        .map { Fixed(it) }

                @Suppress("DEPRECATION")
                val remoteUrlComboBox = comboBox(
                    CollectionComboBoxModel(remoteUrlOptions),
                    listCellRenderer { value -> text = value.getDisplayName() },
                )
                    .columns(COLUMNS_MEDIUM)
                    .comment(message("preferences.repositories.tree.node.linked.remote.url.comment"))
                    .bindItem(RemoteUrlProperty(repository))
                    .component

                remoteUrlOptions.forEach { option ->
                    val remoteUrl = when (option) {
                        is Auto -> repository.getRemoteUrls().first()
                        is Fixed -> option.url
                    }
                    bbRepos[remoteUrl]?.let { repo ->
                        browserLink(
                            message("preferences.repositories.tree.node.linked.remote.url.test.link"),
                            repo.baseUri.toString(),
                        ).visibleIf(remoteUrlComboBox.selectedValueMatches { it == option })
                    }
                }
            }

            row(message("preferences.repositories.tree.node.linked.pr.default.branch.label")) {
                textField()
                    .columns(COLUMNS_MEDIUM)
                    .comment(message("preferences.repositories.tree.node.linked.pr.default.branch.comment"))
                    // TODO 2022.2 introduced .bindText(property) syntax
                    .bind(JTextComponent::getText, JTextComponent::setText, PrBranchNameProperty(repository))
                    .component.apply {
                        emptyText.text =
                            message("preferences.repositories.tree.node.linked.pr.default.branch.empty.text")
                    }
            }

            feedbackFooter()
        }.withBorder(JBEmptyBorder(UIUtil.getRegularPanelInsets()))
    }

    override fun createOptionsPanel(): JComponent = panel

    override fun isModified(): Boolean = panel.isModified()

    override fun apply() {
        panel.apply()
    }
}

sealed interface RemoteUrlOption {
    fun getDisplayName(): String

    object Auto : RemoteUrlOption {
        override fun getDisplayName() = message("preferences.repositories.tree.node.linked.remote.url.option.auto.label")
    }

    data class Fixed(val url: RemoteUrl) : RemoteUrlOption {
        override fun getDisplayName() = url.serialize()
    }
}

private class RemoteUrlProperty(private val repository: Repository) : MutableProperty<RemoteUrlOption?> {
    override fun get(): RemoteUrlOption =
        repository.preferences()
            .getProperty(alwaysLinkToSelectedRemote)
            ?.let { RemoteUrl.parse(it) }
            ?.let { Fixed(it) }
            ?: Auto

    override fun set(value: RemoteUrlOption?) {
        when (value) {
            null, is Auto -> repository.preferences().removeProperty(alwaysLinkToSelectedRemote)
            is Fixed -> repository.preferences().setProperty(alwaysLinkToSelectedRemote, value.url.serialize())
        }
    }
}

private class PrBranchNameProperty(private val repository: Repository) : MutableProperty<String?> {
    override fun get(): String? = repository.preferences().getProperty(pullRequestDefaultTargetBranchName, "")

    override fun set(value: String?) {
        if (value.isNullOrBlank()) {
            repository.preferences().removeProperty(pullRequestDefaultTargetBranchName)
        } else {
            repository.preferences().setProperty(pullRequestDefaultTargetBranchName, value.trim())
        }
    }
}
