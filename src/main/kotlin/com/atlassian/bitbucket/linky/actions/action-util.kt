package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.clipboard.TransferableLink
import com.atlassian.bitbucket.linky.stats.UsageLog
import com.atlassian.bitbucket.linky.stats.cloudEventAttributes
import com.atlassian.bitbucket.linky.stats.serverEventAttributes
import com.intellij.ide.BrowserUtil
import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.ActionPlaces
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.service
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.wm.WindowManager
import com.intellij.ui.awt.RelativePoint
import kotlinx.coroutines.delay
import java.net.URI
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

data class Link(val uri: URI, val text: String)

fun copyToClipboard(project: Project, link: Link, warn: Boolean = false) {
    CopyPasteManager.getInstance().setContents(TransferableLink(link.uri, link.text))
    val messageType = if (warn) MessageType.WARNING else MessageType.INFO
    showStatusBarPopupMessage(project, messageType, "Link to <b>${link.text}</b> has been copied")
}

fun logActionUsage(
    name: String,
    place: String,
    repository: BitbucketRepository,
    attributes: Map<String, Any> = emptyMap()
) {
    val bitbucket = when (repository) {
        is BitbucketRepository.Cloud -> cloudEventAttributes
        is BitbucketRepository.Server -> serverEventAttributes
    }
    service<UsageLog>().event(name, place, attributes + bitbucket)
}

suspend fun openUriInBrowser(project: Project, link: Link, warn: Boolean = false) {
    val messageType = if (warn) MessageType.WARNING else MessageType.INFO
    showStatusBarPopupMessage(project, messageType, message("action.open.link.in.browser.popup.text", link.text))
    val delayDuration = if (warn) 2.seconds else Duration.ZERO
    delay(delayDuration)
    BrowserUtil.browse(link.uri)
}

fun showStatusBarPopupMessage(project: Project, messageType: MessageType, message: String) {
    ApplicationManager.getApplication().invokeLater {
        WindowManager.getInstance()
            .getStatusBar(project)
            .component
            ?.let { statusBar ->
                JBPopupFactory.getInstance()
                    .createHtmlTextBalloonBuilder(message, messageType, null)
                    .setFadeoutTime(5000)
                    .createBalloon()
                    .show(
                        RelativePoint.getCenterOf(statusBar),
                        Balloon.Position.atRight
                    )
            }
    }
}

fun showLogInFileManager() = triggerAction("ShowLog")

fun triggerAction(action: AnAction) {
    DataManager.getInstance().dataContextFromFocusAsync
        .onSuccess { ctx ->
            val actionManager = ActionManager.getInstance()
            val actionEvent = AnActionEvent(null, ctx, ActionPlaces.UNKNOWN, Presentation(), actionManager, 0)
            action.actionPerformed(actionEvent)
        }
}

private fun triggerAction(actionId: String) {
    ActionManager.getInstance().getAction(actionId)?.let { triggerAction(it) }
}
