package com.atlassian.bitbucket.linky.actions

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.atlassian.bitbucket.linky.pipelines.yaml.schema.PipelinesPreferences
import com.atlassian.bitbucket.linky.rest.cloud.config.OAuth3LOConfigurer
import com.atlassian.bitbucket.linky.rest.cloud.config.OAuthTokensHolder
import com.intellij.dvcs.push.ui.VcsPushDialog
import com.intellij.dvcs.repo.Repository
import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.notification.NotificationGroup
import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project

val notificationGroup: NotificationGroup =
    NotificationGroupManager.getInstance().getNotificationGroup("linky.balloon")
val stickyNotificationGroup: NotificationGroup =
    NotificationGroupManager.getInstance().getNotificationGroup("linky.sticky.balloon")
private val displayedIds = mutableSetOf<String>()
private const val oauthNotificationDisplayID = "OAUTH_CONFIG_NOTIFICATION"

fun showLinkToLocalRevisionError(project: Project, repository: Repository) {
    stickyNotificationGroup.createNotification(
        message("action.commit.link.local.title"),
        message("action.commit.link.local.text"),
        NotificationType.WARNING
    )
        .addAction(object : NotificationAction("Push changes") {
            override fun actionPerformed(e: AnActionEvent, notification: Notification) {
                VcsPushDialog(project, listOf(repository), repository).show()
            }
        })
        .setIcon(BitbucketLinkyIcons.Bitbucket)
        .setImportant(true)
        .notify(project)
}

fun showNotificationForRepoAuthentication(project: Project) {
    if (!service<PipelinesPreferences>().oathConfigCancelled() && !displayedIds.contains(oauthNotificationDisplayID)) {
        displayedIds.add(oauthNotificationDisplayID)
        stickyNotificationGroup.createNotification(
            message("authentication.required.title"),
            message("oauth.authentication.required.notification.text"),
            NotificationType.WARNING,
        )
            .addAction(object : NotificationAction(message("oauth.authentication.required.configure.button.label")) {
                override fun actionPerformed(e: AnActionEvent, notification: Notification) {
                    val oAuthTokensHolder = service<OAuthTokensHolder>()
                    project.service<OAuth3LOConfigurer>()
                        .configureOAuthFlow()
                        .thenApply { null != it?.also { tokens -> oAuthTokensHolder.saveOAuthTokens(tokens) } }
                    notification.expire()
                }
            })
            .addAction(object : NotificationAction(message("authentication.required.cancel.button.label")) {
                override fun actionPerformed(e: AnActionEvent, notification: Notification) {
                    service<PipelinesPreferences>().setOathConfigCancelled()
                    notification.expire()
                }
            })
            .setIcon(BitbucketLinkyIcons.Bitbucket)
            .setImportant(true)
            .setDisplayId(oauthNotificationDisplayID)
            .notify(project)
    }
}
