package com.atlassian.bitbucket.linky.utils

import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.pipelines.yaml.schema.getLocalSchemaFilePath
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.github.kittinunf.fuel.core.awaitResult
import com.github.kittinunf.fuel.core.deserializers.EmptyDeserializer
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.apache.http.client.utils.URIBuilder
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import kotlin.io.path.createDirectories
import kotlin.io.path.exists

/**
 * Downloads Pipelines JSON schema from Bitbucket and updates the locally stored
 * schema file if the newly downloaded version differs from it.
 *
 * Local copy is stored in `schemas` directory under IntelliJ configuration directory.
 */
class PipelineSchemaStartupActivity : ProjectActivity {
    private val log = logger()

    override suspend fun execute(project: Project) {
        val localSchemaFile = getLocalSchemaFilePath()
        val freshSchemaFile = fetchSchemaFromBitbucket() ?: return
        if (localSchemaFile.differsFrom(freshSchemaFile)) {
            updateLocalSchema(localSchemaFile, freshSchemaFile)
        }
    }

    private fun Path.differsFrom(anotherSchemaFile: Path): Boolean =
        this.exists().not() || Files.mismatch(this, anotherSchemaFile) >= 0

    private suspend fun updateLocalSchema(localSchemaFile: Path, freshSchemaFile: Path) =
        withContext(Dispatchers.IO) {
            localSchemaFile.createContainingDirectory()
            Files.copy(freshSchemaFile, localSchemaFile, StandardCopyOption.REPLACE_EXISTING)
        }

    private fun Path.createContainingDirectory() {
        if (parent.exists().not()) {
            parent.createDirectories()
        }
    }

    private suspend fun fetchSchemaFromBitbucket(): Path? =
        withContext(Dispatchers.IO) {
            val downloadedSchemaFile = Files.createTempFile("bitbucket-pipelines-fresh", ".json")

            val client = configureFuelHttpClient(BitbucketCloud.apiBaseUrl)
            val result = client.download(BitbucketCloud.pipelinesJsonSchemaUrl().toString())
                .fileDestination { _, _ -> downloadedSchemaFile.toFile() }
                // Wait for the file download attempt to complete
                .awaitResult(EmptyDeserializer)

            result.fold(
                success = {
                    log.debug("Downloaded fresh Pipelines JSON schema to $downloadedSchemaFile")
                    downloadedSchemaFile
                },
                failure = {
                    log.info("Failed to download Pipelines JSON schema", it)
                    null
                }
            )
        }

    private fun BitbucketCloud.pipelinesJsonSchemaUrl() =
        URIBuilder(apiBaseUrl)
            .setPathSegments("schemas", "pipelines-configuration")
            .build()
}
