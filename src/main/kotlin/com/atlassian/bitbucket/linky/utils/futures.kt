package com.atlassian.bitbucket.linky.utils

import java.util.concurrent.CompletableFuture

fun <T, C : CompletableFuture<out T>> List<C>.anyMatch(criteria: (T) -> Boolean): CompletableFuture<T> {
    val result = CompletableFuture<T>()
    val whenMatching = { v: T -> if (criteria(v)) result.complete(v) }
    val matchingFutures = this.map { f -> f.thenAccept(whenMatching) }.toTypedArray()
    CompletableFuture.allOf(*matchingFutures)
        .whenComplete { _, e -> result.completeExceptionally(e ?: NoSuchElementException()) }
    return result
}
