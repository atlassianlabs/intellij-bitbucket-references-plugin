package com.atlassian.bitbucket.linky.preferences

import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.extensions.ExtensionPointName

interface RepositoryPreferences {
    fun preferences(repository: Repository): Preferences?

    companion object {
        val EP_NAME =
            ExtensionPointName.create<RepositoryPreferences>("com.atlassian.bitbucket.linky.repositoryPreferences")
    }
}

private object EmptyPreferences : Preferences {
    override fun getProperty(
        key: String,
        defaultValue: String?
    ): String? = null

    override fun setProperty(
        key: String,
        value: String
    ) {}

    override fun removeProperty(key: String) {}
}

fun Repository.preferences(): Preferences =
    RepositoryPreferences.EP_NAME.extensions.firstNotNullOfOrNull { it.preferences(this) }
        ?: EmptyPreferences

fun String.parseKeyValue(): Pair<String, String>? {
    val kv = split('=', limit = 2)
    return when (kv.size) {
        2 -> kv[0].trim() to kv[1].trim()
        else -> null
    }
}
