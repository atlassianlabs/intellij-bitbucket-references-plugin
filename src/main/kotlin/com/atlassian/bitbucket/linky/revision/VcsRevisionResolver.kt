package com.atlassian.bitbucket.linky.revision

import com.atlassian.bitbucket.linky.Revision
import com.intellij.openapi.extensions.ExtensionPointName
import com.intellij.openapi.vcs.history.VcsRevisionNumber

interface VcsRevisionResolver {

    fun resolveVcsRevision(vcsRevisionNumber: VcsRevisionNumber): Revision?

    companion object {
        val EP_NAME =
            ExtensionPointName.create<VcsRevisionResolver>("com.atlassian.bitbucket.linky.vcsRevisionResolver")
    }
}

class DefaultVcsRevisionResolver : VcsRevisionResolver {
    override fun resolveVcsRevision(vcsRevisionNumber: VcsRevisionNumber): Revision? =
        VcsRevisionResolver.EP_NAME.extensions
            .mapNotNull { it.resolveVcsRevision(vcsRevisionNumber) }
            .firstOrNull()
}
