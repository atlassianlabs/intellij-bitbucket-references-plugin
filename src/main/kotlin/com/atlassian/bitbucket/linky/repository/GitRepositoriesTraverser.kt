package com.atlassian.bitbucket.linky.repository

import com.intellij.openapi.project.Project
import git4idea.GitUtil
import git4idea.repo.GitRepository

class GitRepositoriesTraverser : RepositoriesTraverser {
    override fun traverseRepositories(project: Project): Collection<GitRepository> =
        GitUtil.getRepositoryManager(project).repositories
}
