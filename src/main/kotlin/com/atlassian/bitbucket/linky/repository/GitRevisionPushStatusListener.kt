package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryChangeListener

class GitRevisionPushStatusListener : GitRepositoryChangeListener {
    override fun repositoryChanged(repository: GitRepository) {
        service<ProgressManager>().runProcessWithProgressAsynchronously(
            object : Task.Backgroundable(repository.project, message("checking.push.status.text")) {
                override fun run(indicator: ProgressIndicator) {
                    updateRevisionPushStatus(repository)
                }
            },
            EmptyProgressIndicator(ModalityState.NON_MODAL),
        )
    }

    private fun updateRevisionPushStatus(repository: GitRepository) {
        val project = repository.project
        val lineHandler =
            GitLineHandler(project, repository.root, GitCommand.LOG).apply {
                addParameters("--branches", "--not", "--remotes", "--pretty=format:%H")
            }
        val result = Git.getInstance().runCommand(lineHandler)
        if (result.success()) {
            project
                .service<RevisionPushStatusService>()
                .registerOutgoingRevisions(repository.root, result.output.toSet())
        }
    }
}
