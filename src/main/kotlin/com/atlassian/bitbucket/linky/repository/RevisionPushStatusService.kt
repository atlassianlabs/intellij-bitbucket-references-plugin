package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.Revision
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.vfs.VirtualFile
import java.util.concurrent.ConcurrentHashMap

class RevisionPushStatusService {
    private val outgoingRevisionsMap = ConcurrentHashMap<VirtualFile, Set<Revision>>()

    fun hasRevisionBeenPushed(repository: Repository, revision: Revision): Boolean =
        outgoingRevisionsMap[repository.root]
            ?.contains(revision)
            ?.not()
            ?: true

    fun registerOutgoingRevisions(repositoryRoot: VirtualFile, revisions: Set<Revision>) {
        outgoingRevisionsMap[repositoryRoot] = revisions
    }
}
