package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.intellij.dvcs.repo.Repository
import git4idea.GitUtil
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository

class GitRepositoryExtensionsProvider : RepositoryExtensionsProvider {
    override fun getExtensions(repository: Repository): RepositoryExtensions? =
        when (repository) {
            is GitRepository -> GitRepositoryExtensions(repository)
            else -> null
        }
}

private class GitRepositoryExtensions(private val repository: GitRepository) : RepositoryExtensions {
    override fun getCurrentRemoteUrl(): RemoteUrl? =
        getMainRemote()
            ?.pushUrls
            ?.first()
            ?.let { GitRemoteUrlParser.parseRemoteUrl(it) }

    override fun getRemoteUrls(): List<RemoteUrl> =
        repository.remotes
            .flatMap { it.pushUrls }
            .distinct()
            .mapNotNull { GitRemoteUrlParser.parseRemoteUrl(it) }

    private fun getMainRemote(): GitRemote? {
        val trackInfo = GitUtil.getTrackInfoForCurrentBranch(repository)
        if (trackInfo != null) {
            return trackInfo.remote
        }

        val defaultRemote = GitUtil.getDefaultRemote(repository.remotes)
        if (defaultRemote != null) {
            return defaultRemote
        }

        return null
    }
}
