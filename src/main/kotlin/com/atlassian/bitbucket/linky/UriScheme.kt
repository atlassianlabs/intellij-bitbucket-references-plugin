package com.atlassian.bitbucket.linky

enum class UriScheme constructor(val presentation: String) {
    FILE("file"),
    GIT("git"),
    HTTP("http"),
    HTTPS("https"),
    SSH("ssh");

    override fun toString(): String {
        return presentation
    }

    companion object {
        fun forName(presentation: String?): UriScheme? =
            values().find { it.presentation.equals(presentation, ignoreCase = true) }
    }
}
