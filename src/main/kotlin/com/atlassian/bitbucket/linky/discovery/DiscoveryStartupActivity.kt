package com.atlassian.bitbucket.linky.discovery

import com.intellij.openapi.Disposable
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity
import com.intellij.openapi.util.Disposer
import com.intellij.util.concurrency.AppExecutorUtil
import java.time.Duration
import java.util.concurrent.TimeUnit

class DiscoveryStartupActivity : ProjectActivity {
    override suspend fun execute(project: Project) {
        val schedule = AppExecutorUtil.getAppScheduledExecutorService()
            .scheduleWithFixedDelay(
                { project.discoverRepositories() },
                0,
                Duration.ofMinutes(10).toMinutes(),
                TimeUnit.MINUTES
            )

        Disposer.register(project, Disposable { schedule.cancel(true) })
    }
}
