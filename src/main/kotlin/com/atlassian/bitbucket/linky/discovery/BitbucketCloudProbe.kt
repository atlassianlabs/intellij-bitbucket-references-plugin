package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud

private val log = logger()

object BitbucketCloudProbe {
    fun probeBitbucketCloud(detectedHost: String): BitbucketCloud? =
        if (detectedHost == BitbucketCloud.baseUrl.host) {
            BitbucketCloud
        } else {
            log.debug("Candidate host doesn't look like Bitbucket Cloud '$detectedHost'")
            null
        }
}
