package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.net.URI
import java.util.concurrent.CompletableFuture

class MirrorApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketServer,
) : MirrorApi {
    override fun upstreamServer(): CompletableFuture<MirrorInfo> {
        val future = CompletableFuture<MirrorInfo>()
        fuel.get(instance.mirrorUpstreamServersUrl.toString())
            .responseObject<PagedBean<MirrorInfo>>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(it.values.single()) },
                    failure = { future.completeExceptionally(it.mapException("mirror upstream server")) }
                )
            }
        return future
    }
}

private val BitbucketServer.mirrorUpstreamServersUrl: URI
    get() = apiBaseUrl.resolve("rest/mirroring/1.0/upstreamServers")
