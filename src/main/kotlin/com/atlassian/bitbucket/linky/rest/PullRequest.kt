package com.atlassian.bitbucket.linky.rest

import java.net.URI
import java.time.ZonedDateTime

data class PullRequest(
    val id: Long,
    val title: String,
    val description: String?,
    val sourceBranchName: String,
    val destinationBranchName: String,
    val state: String,
    val authorName: String?,
    val createdDate: ZonedDateTime,
    val link: URI
)
