package com.atlassian.bitbucket.linky.rest.cloud

import java.net.URI

data class HtmlLinksBean(val html: LinkBean) {
    fun uri(): URI = URI.create(html.href)
}

data class AvatarLinksBean(val avatar: LinkBean) {
    fun uri(): URI = URI.create(avatar.href)
}

data class LinkBean(val href: String)
