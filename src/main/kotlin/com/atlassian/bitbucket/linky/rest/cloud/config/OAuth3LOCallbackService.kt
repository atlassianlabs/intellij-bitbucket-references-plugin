package com.atlassian.bitbucket.linky.rest.cloud.config

import com.atlassian.bitbucket.linky.logger
import fi.iki.elonen.NanoHTTPD
import fi.iki.elonen.NanoHTTPD.Response.Status.BAD_REQUEST
import fi.iki.elonen.NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED
import fi.iki.elonen.NanoHTTPD.Response.Status.NOT_FOUND
import fi.iki.elonen.NanoHTTPD.Response.Status.OK
import java.time.Duration
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

private const val oauth3LoCallbackPort = 23945
private val oauth3LoCallbackTimeout = Duration.ofMinutes(15)
private val httpServerShutdownTimeout = Duration.ofSeconds(20)

private val log = logger()

object OAuth3LOCallbackService {
    private val callbackRegistry = mutableMapOf<String, OAuth3LOCallback>()
    private val executor = Executors.newSingleThreadScheduledExecutor()
    private val httpServer = AtomicReference<HttpServer>()

    fun expectCallback(callback: OAuth3LOCallback) {
        val existingCallback = callbackRegistry.putIfAbsent(callback.id, callback)
        check(existingCallback == null || existingCallback == callback) {
            "Callback with ID '$callback.id' already registered"
        }
        executor.schedule(
            {
                cancelCallback(
                    callback
                )
            },
            oauth3LoCallbackTimeout.toMillis(),
            TimeUnit.MILLISECONDS
        )

        if (httpServer.compareAndSet(
                null,
                HttpServer { provideCallback(it) }
            )
        ) {
            httpServer.get().start()
        }
    }

    fun cancelCallback(callback: OAuth3LOCallback) {
        callbackRegistry.remove(callback.id)
        if (callbackRegistry.isEmpty()) {
            executor.schedule(
                { shutdownHttpServerIfPossible() },
                httpServerShutdownTimeout.toMillis(),
                TimeUnit.MILLISECONDS
            )
        }
    }

    private fun provideCallback(id: String): OAuth3LOCallback? {
        val delegate = callbackRegistry[id] ?: return null

        return object : OAuth3LOCallback {
            override val id = delegate.id
            override fun onSuccess(code: String) = delegate.onSuccess(code).also {
                cancelCallback(
                    delegate
                )
            }

            override fun onDecline() = delegate.onDecline().also {
                cancelCallback(
                    delegate
                )
            }

            override fun onError(description: String?) = delegate.onError(description).also {
                cancelCallback(
                    delegate
                )
            }
        }
    }

    private fun shutdownHttpServerIfPossible() {
        if (callbackRegistry.isNotEmpty()) return

        val server = httpServer.get()
        if (server != null && httpServer.compareAndSet(server, null)) {
            server.stop()
        }
    }
}

private class HttpServer(
    private val callbackProvider: (String) -> OAuth3LOCallback?
) : NanoHTTPD("localhost", oauth3LoCallbackPort) {

    override fun serve(session: IHTTPSession): Response {
        if (session.uri.trimEnd('/') != "/linky/oauth") {
            return replyWith(NOT_FOUND, "not_found")
        }
        if (session.method != Method.GET) {
            return newFixedLengthResponse(
                METHOD_NOT_ALLOWED,
                MIME_PLAINTEXT,
                "Expected GET request"
            )
        }

        val params = session.parameters

        val stateToken = params["state"]?.firstOrNull()
            ?: return replyWith(BAD_REQUEST, "bad_request").also {
                log.warn("OAuth 3LO callback service: expected 'state' query parameter")
            }

        val callback = callbackProvider(stateToken)
            ?: return replyWith(BAD_REQUEST, "bad_request").also {
                log.warn("OAuth 3LO callback service: unexpected OAuth callback")
            }

        val error = params["error"]?.firstOrNull()
        if (error != null) {
            return when (error) {
                "access_denied" -> {
                    callback.onDecline()
                    replyWith(OK, "authorization_declined")
                }
                else -> {
                    callback.onError(params["error_description"]?.firstOrNull())
                    replyWith(BAD_REQUEST, "authorization_failed")
                }
            }
        } else {
            val code = params["code"]?.firstOrNull()
                ?: return replyWith(BAD_REQUEST, "bad_request").also {
                    log.warn("OAuth 3LO callback service: expected 'code' query parameter")
                }
            // TODO analyze if code is valid
            callback.onSuccess(code)
        }

        return replyWith(OK, "success")
    }

    private fun replyWith(status: Response.Status, template: String) =
        newChunkedResponse(
            status,
            MIME_HTML,
            javaClass.classLoader
                .getResourceAsStream("html/oauth/$template.html")
        )
}
