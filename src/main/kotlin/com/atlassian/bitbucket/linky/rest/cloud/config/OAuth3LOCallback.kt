package com.atlassian.bitbucket.linky.rest.cloud.config

interface OAuth3LOCallback {

    val id: String

    fun onSuccess(code: String)

    fun onDecline()

    fun onError(description: String?)
}
