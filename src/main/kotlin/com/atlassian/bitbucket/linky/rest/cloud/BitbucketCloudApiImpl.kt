package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.auth.Authentication.AccessToken
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Basic
import com.atlassian.bitbucket.linky.rest.auth.Authentication.OAuth
import com.atlassian.bitbucket.linky.rest.auth.basicAuthInterceptor
import com.atlassian.bitbucket.linky.rest.cloud.config.defaultGson
import com.atlassian.bitbucket.linky.rest.cloud.oauth.BitbucketCloudOAuthApiImpl
import com.atlassian.bitbucket.linky.rest.cloud.oauth.OAuthInterceptors
import com.github.kittinunf.fuel.core.FuelManager
import com.google.gson.Gson

class BitbucketCloudApiImpl(
    private val createFuel: () -> FuelManager = { FuelManager() },
    private val gson: Gson = defaultGson(),
    customConfig: BitbucketCloudApiConfig.() -> Unit = { }
) : BitbucketCloudApi {

    private val config = BitbucketCloudApiConfig().apply(customConfig)
    private val fuel = createFuel()

    init {
        with(config) {
            when (val auth = authentication) {
                is Basic -> {
                    fuel.addRequestInterceptor(basicAuthInterceptor(BitbucketCloud, auth))
                }
                is OAuth -> {
                    val oAuthApi = BitbucketCloudOAuthApiImpl(createFuel, gson) {
                        consumer = auth.consumer
                    }
                    val interceptors = OAuthInterceptors(oAuthApi, auth.tokenManager)
                    fuel.addRequestInterceptor(interceptors.requestInterceptor())
                    fuel.addResponseInterceptor(interceptors.responseInterceptor())
                }
                is AccessToken -> throw IllegalArgumentException("Bitbucket Cloud doesn't support access token authentication")
                else -> fuel
            }
        }
    }

    override fun workspace(workspaceId: WorkspaceId): WorkspaceApi =
        WorkspaceApiImpl(fuel, gson, workspaceId)

    override fun repository(repositoryId: RepositoryId): RepositoryApi =
        RepositoryApiImpl(fuel, gson, repositoryId)

    override fun snippets(workspaceId: String?): SnippetsApi = SnippetsApiImpl(fuel, gson, workspaceId)
}
