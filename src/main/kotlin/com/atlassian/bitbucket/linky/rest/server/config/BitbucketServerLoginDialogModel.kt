package com.atlassian.bitbucket.linky.rest.server.config

import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.server.config.BitbucketServerLoginDialogModel.TokenVerificationState.None
import com.intellij.openapi.observable.properties.AtomicProperty

class BitbucketServerLoginDialogModel() {
    var username: String = ""
    var password: String = ""
    var tokenName: String = ""
    var token: String = ""
    var verificationState = AtomicProperty<TokenVerificationState>(None)

    sealed class TokenVerificationState {
        data object None : TokenVerificationState()
        data object Verifying : TokenVerificationState()
        class Failed(val error: Throwable) : TokenVerificationState()
        class Verified(val token: PersonalAccessToken) : TokenVerificationState()
    }
}
