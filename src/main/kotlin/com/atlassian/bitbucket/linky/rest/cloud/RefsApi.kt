package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import org.apache.http.client.utils.URIBuilder
import java.net.URI
import java.util.concurrent.CompletableFuture

interface RefsApi {
    fun list(matching: String, pageNumber: Int = 1): CompletableFuture<CloudPage<Reference>>
}

data class Reference(val name: String, val type: Type) {
    enum class Type { BRANCH, TAG, UNKNOWN }
}

class RefsApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val repositoryId: RepositoryId,
) : RefsApi {
    private val refsFields = listOf("values.name", "values.type")

    override fun list(matching: String, pageNumber: Int): CompletableFuture<CloudPage<Reference>> {
        val future = CompletableFuture<CloudPage<Reference>>()

        fuel.get(BitbucketCloud.refsUrl(matching, pageNumber).toString())
            .responseObject<PagedBean<RefBean>>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(it.toRefPage(matching)) },
                    failure = { future.completeExceptionally(it.mapException("get refs")) },
                )
            }

        return future
    }

    private fun PagedBean<RefBean>.toRefPage(matching: String): CloudPage<Reference> {
        fun pageFetcher(pageNumber: Int) = { api: BitbucketCloudApi ->
            api.repository(repositoryId).refs().list(matching, pageNumber)
        }
        return CloudPage(
            items = values.map { it.toReference() },
            pageSize = pagelen,
            pageNumber = page,
            nextPage = next?.let { pageFetcher(page + 1) }
        )
    }

    private fun BitbucketCloud.refsUrl(matching: String, pageNumber: Int): URI {
        val sanitizedMatching = matching.replace("\"", "")
        return URIBuilder(apiBaseUrl)
            .setPathSegments(
                "2.0",
                "repositories",
                repositoryId.workspace,
                repositoryId.slug,
                "refs"
            )
            .addParameter(
                "fields",
                (PagedBean.paginationFields + refsFields).joinToString(separator = ","),
            )
            .addParameter(
                "q",
                "name ~ \"$sanitizedMatching\"",
            )
            .addParameter("pagelen", "100")
            .addParameter("page", "$pageNumber")
            .addParameter("sort", "name")
            .build()
    }
}

private data class RefBean(val name: String, val type: String) {
    fun toReference() = Reference(
        name,
        Reference.Type.entries
            .find { it.name.equals(type, ignoreCase = true) }
            ?: Reference.Type.UNKNOWN
    )
}
