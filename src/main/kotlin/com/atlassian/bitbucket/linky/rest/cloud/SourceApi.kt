package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import org.apache.http.client.utils.URIBuilder
import java.net.URI
import java.util.concurrent.CompletableFuture

interface SourceApi {
    fun atRef(ref: String): SourceAtRefApi
}

interface SourceAtRefApi {
    fun getFileContent(filename: String): CompletableFuture<ByteArray>
}

class SourceApiImpl(
    private val fuel: FuelManager,
    private val repositoryId: RepositoryId,
) : SourceApi {
    override fun atRef(ref: String): SourceAtRefApi = SourceApiAtRefImpl(fuel, repositoryId, ref)
}

class SourceApiAtRefImpl(
    private val fuel: FuelManager,
    private val repositoryId: RepositoryId,
    private val ref: String,
) : SourceAtRefApi {
    override fun getFileContent(filename: String): CompletableFuture<ByteArray> {
        val future = CompletableFuture<ByteArray>()

        fuel.get(BitbucketCloud.fileUrl(filename).toString())
            .response { _, _, result ->
                result.fold(
                    success = { future.complete(it) },
                    failure = { future.completeExceptionally(it.mapException("get file content")) },
                )
            }

        return future
    }

    private fun BitbucketCloud.fileUrl(filename: String): URI =
        URIBuilder(apiBaseUrl)
            .setPathSegments(
                "2.0",
                "repositories",
                repositoryId.workspace,
                repositoryId.slug,
                "src",
                ref,
                filename
            )
            .build()
}
