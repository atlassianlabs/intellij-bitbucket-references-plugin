package com.atlassian.bitbucket.linky.rest.server.config

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.rest.ForbiddenException
import com.atlassian.bitbucket.linky.rest.UnauthorizedException
import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.AccessToken
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Basic
import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.atlassian.bitbucket.linky.rest.server.AccessLevel.READ
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApi
import com.atlassian.bitbucket.linky.rest.server.BitbucketServerApiImpl
import com.atlassian.bitbucket.linky.rest.server.Permission.ProjectPermission
import com.atlassian.bitbucket.linky.rest.server.Permission.RepositoryPermission
import com.atlassian.bitbucket.linky.rest.server.config.BitbucketServerLoginDialogModel.TokenVerificationState.Failed
import com.atlassian.bitbucket.linky.rest.server.config.BitbucketServerLoginDialogModel.TokenVerificationState.Verified
import com.atlassian.bitbucket.linky.rest.server.config.BitbucketServerLoginDialogModel.TokenVerificationState.Verifying
import com.atlassian.bitbucket.linky.rest.server.config.DialogMode.BasicAuth
import com.atlassian.bitbucket.linky.rest.server.config.DialogMode.Token
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.observable.properties.AtomicProperty
import com.intellij.openapi.observable.util.equalsTo
import com.intellij.openapi.observable.util.notEqualsTo
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.AnimatedIcon
import com.intellij.ui.components.fields.ExtendableTextComponent
import com.intellij.ui.components.fields.ExtendableTextField
import com.intellij.ui.dsl.builder.AlignX
import com.intellij.ui.dsl.builder.Panel
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

class BitbucketServerLoginDialog(
    private val project: Project,
    private val server: BitbucketServer,
    private val model: BitbucketServerLoginDialogModel,
) : DialogWrapper(project, null, false, IdeModalityType.IDE) {
    private val serverTextField = ExtendableTextField().apply {
        isEditable = false
        isFocusable = false
        text = server.baseUrl.toString()
    }
    private val progressExtension =
        ExtendableTextComponent.Extension
            .create(
                AnimatedIcon.Default(),
                message("personal.access.token.dialog.progress.tooltip"),
                null
            )
    private val dialogMode: AtomicProperty<DialogMode> = AtomicProperty(BasicAuth)

    init {
        title = message("personal.access.token.dialog.title")

        model.verificationState
            .afterChange { state ->
                when (state) {
                    Verifying -> serverTextField.addExtension(progressExtension)
                    else -> {
                        serverTextField.removeExtension(progressExtension)
                        initValidation()
                    }
                }
            }

        init()
    }

    override fun createCenterPanel(): JComponent =
        panel {
            panel {
                row(message("personal.access.token.dialog.server.label")) {
                    cell(serverTextField)
                        .align(AlignX.FILL)
                        .resizableColumn()
                        .enabledIf(model.verificationState.notEqualsTo(Verifying))
                }
                createBasicAuthPanel()
                createTokenPanel()
            }
        }

    override fun createSouthAdditionalPanel(): JPanel {
        return panel {
            row {
                link(message("personal.access.token.dialog.switch.to.token.mode.text")) {
                    dialogMode.set(Token)
                }
            }.visibleIf(dialogMode.equalsTo(BasicAuth))
            row {
                link(message("personal.access.token.dialog.switch.to.basic.auth.mode.text")) {
                    dialogMode.set(BasicAuth)
                }
            }.visibleIf(dialogMode.equalsTo(Token))
        }
    }

    override fun doOKAction() {
        applyFields()
        if (isOKActionEnabled) {
            validateToken()
        }
    }

    override fun doValidate(): ValidationInfo? {
        val error =
            (model.verificationState.get() as? Failed)
                ?.error
                ?: return null

        return when (error) {
            is ForbiddenException ->
                ValidationInfo(
                    message("personal.access.token.dialog.verification.insufficient.access.level.error.text")
                )
            is UnauthorizedException ->
                ValidationInfo(
                    message("personal.access.token.dialog.verification.incorrect.credentials.error.text")
                ).withOKEnabled()
            else ->
                ValidationInfo(
                    error.message ?: message("personal.access.token.dialog.verification.generic.error.text")
                ).withOKEnabled()
        }
    }

    private fun Panel.createBasicAuthPanel() =
        rowsRange {
            row(message("personal.access.token.dialog.username.label")) {
                textField()
                    .bindText(model::username)
                    .align(AlignX.FILL)
                    .resizableColumn()
                    .focused()
                    .errorOnApply(message("personal.access.token.dialog.username.empty.error.text")) {
                        it.isVisibleAndBlank()
                    }
            }
            row(message("personal.access.token.dialog.password.label")) {
                passwordField()
                    .bindText(model::password)
                    .align(AlignX.FILL)
                    .resizableColumn()
                    .errorOnApply(message("personal.access.token.dialog.password.empty.error.text")) {
                        it.isVisible && it.password.isEmpty()
                    }
            }
            row(message("personal.access.token.dialog.token.name.label")) {
                textField()
                    .applyToComponent {
                        emptyText.text = message("personal.access.token.dialog.token.name.default.value")
                    }
                    .bindText(model::tokenName)
                    .align(AlignX.FILL)
                    .resizableColumn()
            }
            row { comment(message("personal.access.token.dialog.token.note.text")) }
        }
            .visibleIf(dialogMode.equalsTo(BasicAuth))
            .enabledIf(model.verificationState.notEqualsTo(Verifying))

    private fun Panel.createTokenPanel() =
        row(message("personal.access.token.dialog.token.label")) {
            textField()
                .bindText(model::token)
                .align(AlignX.FILL)
                .resizableColumn()
                .focused()
                .errorOnApply(message("personal.access.token.dialog.token.empty.error.text")) {
                    it.isVisibleAndBlank()
                }
        }
            .visibleIf(dialogMode.equalsTo(Token))
            .enabledIf(model.verificationState.notEqualsTo(Verifying))

    private fun validateToken() {
        model.verificationState.set(Verifying)

        service<ProgressManager>().runProcessWithProgressAsynchronously(
            object : Task.Backgroundable(project, "Not visible") {
                override fun run(indicator: ProgressIndicator) {
                    dialogMode.get()
                        .acquireToken(server, model)
                        .thenApply { model.verificationState.set(Verified(it)) }
                        .get(10, TimeUnit.SECONDS)
                }

                override fun onSuccess() {
                    if (model.verificationState.get() is Verified) {
                        close(OK_EXIT_CODE)
                    }
                }

                override fun onThrowable(error: Throwable) {
                    model.verificationState.set(Failed(error))
                }
            },
            EmptyProgressIndicator(ModalityState.stateForComponent(window)),
        )
    }
}

private sealed class DialogMode {
    abstract fun acquireToken(
        server: BitbucketServer,
        model: BitbucketServerLoginDialogModel,
    ): CompletableFuture<PersonalAccessToken>

    protected fun createClient(server: BitbucketServer, auth: Authentication): BitbucketServerApi {
        return BitbucketServerApiImpl(createFuel = { configureFuelHttpClient(server.baseUrl) }) {
            instance = server
            authentication = auth
        }
    }

    data object BasicAuth : DialogMode() {
        override fun acquireToken(
            server: BitbucketServer,
            model: BitbucketServerLoginDialogModel,
        ): CompletableFuture<PersonalAccessToken> {
            val client = createClient(server, Basic(model.username, model.password))
            val tokenName = model.tokenName
                .ifEmpty { message("personal.access.token.dialog.token.name.default.value") }

            return client.personalAccessTokens(model.username)
                .create(tokenName, listOf(ProjectPermission(READ), RepositoryPermission(READ)))
        }
    }

    data object Token : DialogMode() {
        override fun acquireToken(
            server: BitbucketServer,
            model: BitbucketServerLoginDialogModel,
        ): CompletableFuture<PersonalAccessToken> {
            val token = PersonalAccessToken(model.token)
            val client = createClient(server, AccessToken(token))

            return client.testConnectivity().authenticatedResource()
                .thenApply { token }
        }
    }
}

private fun JTextField.isVisibleAndBlank() = isVisible && text.isBlank()
