package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import org.apache.http.client.utils.URIBuilder
import java.net.URI
import java.util.concurrent.CompletableFuture

interface WorkspaceApi {
    fun repositories(
        matching: String = "",
        pageNumber: Int = 1,
    ): CompletableFuture<CloudPage<Repository>>
}

data class Repository(val name: String, val slug: String)

class WorkspaceApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val workspaceId: WorkspaceId,
) : WorkspaceApi {
    private val repositoriesFields = listOf("values.slug", "values.name")

    override fun repositories(
        matching: String,
        pageNumber: Int,
    ): CompletableFuture<CloudPage<Repository>> {
        val future = CompletableFuture<CloudPage<Repository>>()

        fuel.get(
            BitbucketCloud.workspaceRepositoriesUrl(workspaceId, matching, pageNumber).toString()
        ).responseObject<PagedBean<RepositoryBean>>(gson) { _, _, result ->
            result.fold(
                success = { future.complete(it.toRepositoryPage(matching)) },
                failure = { future.completeExceptionally(it.mapException("get repositories")) },
            )
        }

        return future
    }

    private fun PagedBean<RepositoryBean>.toRepositoryPage(
        matching: String,
    ): CloudPage<Repository> {
        fun pageFetcher(pageNumber: Int) = { api: BitbucketCloudApi ->
            api.workspace(workspaceId).repositories(matching, pageNumber)
        }
        return CloudPage(
            items = values.map { it.toRepository() },
            pageSize = pagelen,
            pageNumber = page,
            nextPage = next?.let { pageFetcher(page + 1) }
        )
    }

    private fun BitbucketCloud.workspaceRepositoriesUrl(
        workspaceId: WorkspaceId,
        matching: String,
        pageNumber: Int,
    ): URI {
        val sanitizedMatching = matching.replace("\"", "")
        return URIBuilder(apiBaseUrl)
            .setPathSegments(
                "2.0",
                "repositories",
                workspaceId.workspace,
            )
            .addParameter(
                "fields",
                (PagedBean.paginationFields + repositoriesFields).joinToString(separator = ","),
            )
            .addParameter(
                "q",
                "(slug ~ \"$sanitizedMatching\" OR name ~ \"$sanitizedMatching\")",
            )
            .addParameter("pagelen", "100")
            .addParameter("page", "$pageNumber")
            .build()
    }
}

private data class RepositoryBean(val name: String, val slug: String) {
    fun toRepository() = Repository(name, slug)
}
