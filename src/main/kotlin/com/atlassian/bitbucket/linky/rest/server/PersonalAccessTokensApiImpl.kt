package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.jsonBody
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.util.concurrent.CompletableFuture

class PersonalAccessTokensApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketServer,
    private val username: String
) : PersonalAccessTokensApi {

    override fun create(name: String, permissions: Iterable<Permission>): CompletableFuture<PersonalAccessToken> {
        val future = CompletableFuture<PersonalAccessToken>()
        fuel.put(instance.personalAccessTokensUrl(username).toString())
            .jsonBody(CreatePersonalAccessTokenBean(name, permissions.map { it.getCode() }), gson)
            .responseObject<PersonalAccessTokenBean>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(it.toPersonalAccessToken()) },
                    failure = { future.completeExceptionally(it.mapException("create personal access token")) }
                )
            }
        return future
    }
}

private fun BitbucketServer.personalAccessTokensUrl(username: String) =
    apiBaseUrl.resolve("rest/access-tokens/1.0/users/$username")

private data class CreatePersonalAccessTokenBean(
    val name: String,
    val permissions: List<String>
)

private data class PersonalAccessTokenBean(
    val id: String,
    val name: String,
    val token: String,
    val permissions: List<String>
) {
    fun toPersonalAccessToken() = PersonalAccessToken(token)
}
