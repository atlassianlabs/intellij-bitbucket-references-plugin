package com.atlassian.bitbucket.linky.rest.server.config

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.rest.server.config.BitbucketServerLoginDialogModel.TokenVerificationState.Verified
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageDialogBuilder
import java.util.concurrent.CompletableFuture

interface PersonalAccessTokenConfigurer {
    fun configurePersonalAccessToken(
        server: BitbucketServer,
        actionDescription: String
    ): CompletableFuture<PersonalAccessToken?>
}

class DefaultPersonalAccessTokenConfigurer(private val project: Project) : PersonalAccessTokenConfigurer {

    override fun configurePersonalAccessToken(
        server: BitbucketServer,
        actionDescription: String
    ): CompletableFuture<PersonalAccessToken?> {
        val future = CompletableFuture<PersonalAccessToken?>()

        ApplicationManager.getApplication().invokeLater ui@{
            if (userWantsToConfigureAuth(actionDescription)) {
                val dialogModel = BitbucketServerLoginDialogModel()
                val dialog = BitbucketServerLoginDialog(project, server, dialogModel)
                if (dialog.showAndGet()) {
                    (dialogModel.verificationState.get() as? Verified)
                        ?.token
                        ?.let { personalAccessToken ->
                            val personalAccessTokenHolder = service<PersonalAccessTokenHolder>()
                            personalAccessTokenHolder.savePersonalAccessToken(server, personalAccessToken)
                            future.complete(personalAccessToken)
                            return@ui
                        }
                }
            }
            future.complete(null)
        }
        return future
    }

    private fun userWantsToConfigureAuth(actionDescription: String): Boolean =
        MessageDialogBuilder
            .yesNo(
                message("authentication.required.title"),
                message("personal.access.token.authentication.required.text", actionDescription)
            )
            .yesText(message("personal.access.token.authentication.required.configure.button.label"))
            .noText(message("authentication.required.cancel.button.label"))
            .ask(project)
}
