package com.atlassian.bitbucket.linky.rest.auth

data class OAuthConsumer(val id: String, val secret: String)
