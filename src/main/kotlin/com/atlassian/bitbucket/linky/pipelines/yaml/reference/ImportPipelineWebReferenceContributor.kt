package com.atlassian.bitbucket.linky.pipelines.yaml.reference

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.pipelines.yaml.config.PIPELINES_CONFIGURATION_FILENAME
import com.atlassian.bitbucket.linky.pipelines.yaml.config.PipelineDefinition
import com.atlassian.bitbucket.linky.pipelines.yaml.config.SharedPipelineDefinitionsProvider
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.SharedPipelinesPatterns
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.openapi.components.service
import com.intellij.openapi.paths.WebReference
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.PsiReferenceContributor
import com.intellij.psi.PsiReferenceProvider
import com.intellij.psi.PsiReferenceRegistrar
import com.intellij.util.ProcessingContext
import org.apache.http.client.utils.URIBuilder
import org.jetbrains.yaml.psi.YAMLScalar
import java.net.URI

class ImportPipelineWebReferenceContributor : PsiReferenceContributor() {

    override fun registerReferenceProviders(registrar: PsiReferenceRegistrar) {
        registrar.registerReferenceProvider(
            SharedPipelinesPatterns.importStatementPattern,
            ImportPipelineReferenceProvider(),
        )
    }
}

private class ImportPipelineReferenceProvider : PsiReferenceProvider() {
    override fun getReferencesByElement(
        element: PsiElement,
        context: ProcessingContext,
    ): Array<PsiReference> {
        val cloudRepo = getBitbucketRepository(element) ?: return emptyArray()

        val textRange = (element as? YAMLScalar)
            ?.createLiteralTextEscaper()
            ?.takeIf { it.isOneLine }
            ?.relevantTextRange
            ?.takeUnless { it.isEmpty }
            ?: return emptyArray()

        return element.text.split(":", limit = 4)
            .takeIf { parts -> parts.size == 3 && parts.none { it.isBlank() } }
            ?.let { (repoSlug, refName, pipelineName) ->
                val project = element.project
                val workspaceId = cloudRepo.workspaceId

                kickOffImportResolution(project, workspaceId, repoSlug, refName)

                arrayOf(
                    ImportPipelineReference(
                        element,
                        textRange,
                        workspaceId,
                        repoSlug,
                        refName,
                        pipelineName,
                    )
                )
            }
            ?: emptyArray()
    }

    private fun getBitbucketRepository(element: PsiElement): BitbucketRepository.Cloud? {
        val project = element.project
        val file = element.containingFile.virtualFile ?: return null
        val repository = project.service<VcsRepositoryManager>()
            .getRepositoryForFile(file, true)
            ?: return null
        return project.service<BitbucketRepositoriesService>()
            .getBitbucketRepository(repository)
            as? BitbucketRepository.Cloud
    }

    private fun kickOffImportResolution(
        project: Project,
        workspaceId: String,
        repositorySlug: String,
        refName: String,
    ) {
        project.service<SharedPipelineDefinitionsProvider>()
            .getSharedPipelineDefinitions(workspaceId, repositorySlug, refName)
    }
}

private class ImportPipelineReference(
    element: PsiElement,
    textRange: TextRange,
    private val workspaceId: String,
    private val repositorySlug: String,
    private val refName: String,
    private val pipelineName: String,
) : WebReference(element, textRange) {
    override fun resolve(): PsiElement? = url?.let { super.resolve() }
    override fun getUrl(): String? =
        getExternalConfigurationFileUrl(
            element.project,
            workspaceId,
            repositorySlug,
            refName,
            pipelineName,
        )?.toString()

    private fun getExternalConfigurationFileUrl(
        project: Project,
        workspaceId: String,
        repositorySlug: String,
        refName: String,
        pipelineName: String,
    ): URI? =
        project.service<SharedPipelineDefinitionsProvider>()
            .getSharedPipelineDefinitionsIfPresent(workspaceId, repositorySlug, refName)
            ?.let { it.definitions.find { definition -> definition.name == pipelineName } }
            ?.let { definition ->
                getPipelineDefinitionUrl(
                    workspaceId,
                    repositorySlug,
                    refName,
                    definition,
                )
            }

    private fun getPipelineDefinitionUrl(
        workspaceId: String,
        repositorySlug: String,
        refName: String,
        pipelineDefinition: PipelineDefinition,
    ): URI {
        return URIBuilder(BitbucketCloud.baseUrl)
            .setPathSegments(
                workspaceId,
                repositorySlug,
                "src",
                refName,
                PIPELINES_CONFIGURATION_FILENAME,
            )
            .setFragment(with(pipelineDefinition) { "lines-${startLine + 1}:${endLine + 1}" })
            .build()
    }
}
