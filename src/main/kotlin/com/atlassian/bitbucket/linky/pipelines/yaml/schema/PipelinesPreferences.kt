package com.atlassian.bitbucket.linky.pipelines.yaml.schema

import com.atlassian.bitbucket.linky.SETTINGS_STORAGE_FILE
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage

@Service
@State(name = "Pipelines", storages = [Storage(SETTINGS_STORAGE_FILE)])
class PipelinesPreferences : PersistentStateComponent<PipelinesPreferences.State> {
    private var state = State()

    override fun getState(): State = state

    override fun loadState(loadedState: State) {
        state = loadedState
    }

    fun useInternalJsonSchema(): Boolean = state.useInternalJsonSchema

    fun doUseInternalJsonSchema() {
        state.useInternalJsonSchema = true
    }

    fun oathConfigCancelled(): Boolean = state.oathConfigCancelled

    fun setOathConfigCancelled() {
        state.oathConfigCancelled = true
    }

    class State {
        var useInternalJsonSchema: Boolean = false
        var oathConfigCancelled: Boolean = false
    }
}
