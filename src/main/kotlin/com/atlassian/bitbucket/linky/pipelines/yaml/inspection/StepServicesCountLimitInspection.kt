package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.serviceReferencesPattern
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLSequence
import org.jetbrains.yaml.psi.YamlPsiElementVisitor

private const val MAX_SERVICES_IN_STEP = 5

class StepServicesCountLimitInspection : LocalInspectionTool() {
    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitSequence(sequence: YAMLSequence) {
            if (serviceReferencesPattern.accepts(sequence)) {
                if (sequence.items.size > MAX_SERVICES_IN_STEP) {
                    val keyValue = sequence.parent as? YAMLKeyValue ?: return
                    keyValue.key?.let {
                        holder.registerProblem(
                            it,
                            message("inspections.step.services.count.too.many.problem.text"),
                            ProblemHighlightType.GENERIC_ERROR_OR_WARNING
                        )
                    }
                }
            }
        }
    }
}
