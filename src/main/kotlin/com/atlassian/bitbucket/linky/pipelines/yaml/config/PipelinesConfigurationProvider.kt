package com.atlassian.bitbucket.linky.pipelines.yaml.config

import com.atlassian.bitbucket.linky.LinkyBundle
import com.atlassian.bitbucket.linky.pipelines.yaml.completion.AuthenticationNotConfiguredException
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloudApi
import com.atlassian.bitbucket.linky.rest.cloud.RepositoryId
import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.toJavaDuration

const val PIPELINES_CONFIGURATION_FILENAME = "bitbucket-pipelines.yml"

interface PipelinesConfigurationProvider {
    fun getPipelinesConfiguration(
        workspaceId: String,
        repositorySlug: String,
        ref: String,
    ): CompletableFuture<String>

    fun getPipelinesConfigurationIfPresent(
        workspaceId: String,
        repositorySlug: String,
        ref: String,
    ): String?
}

class CachingPipelinesConfigurationProvider(
    private val project: Project,
) : PipelinesConfigurationProvider {
    private val configCache: LoadingCache<CacheKey, String> =
        Caffeine.newBuilder()
            .maximumSize(20)
            .softValues()
            .expireAfterWrite(24.hours.toJavaDuration())
            .refreshAfterWrite(10.minutes.toJavaDuration())
            .build { repoId -> fetchPipelinesConfiguration(repoId) }

    override fun getPipelinesConfiguration(
        workspaceId: String,
        repositorySlug: String,
        ref: String,
    ): CompletableFuture<String> =
        CompletableFuture.supplyAsync {
            configCache.get(CacheKey(workspaceId, repositorySlug, ref))
        }

    override fun getPipelinesConfigurationIfPresent(
        workspaceId: String,
        repositorySlug: String,
        ref: String
    ): String? =
        configCache.getIfPresent(CacheKey(workspaceId, repositorySlug, ref))

    private fun fetchPipelinesConfiguration(
        cacheKey: CacheKey,
    ): String =
        getClient(LinkyBundle.message("common.fetching.pipelines.configuration"))
            .thenCompose { api ->
                api.repository(cacheKey.toRepositoryId())
                    .source()
                    .atRef(cacheKey.ref)
                    .getFileContent(PIPELINES_CONFIGURATION_FILENAME)
            }
            .thenApply { it.decodeToString() }
            .get(10, TimeUnit.SECONDS)

    private fun getClient(operationDescription: String): CompletableFuture<BitbucketCloudApi> {
        val restClientProvider = project.service<BitbucketRestClientProvider>()
        if (!restClientProvider.isCloudAuthenticationConfigured()) {
            throw AuthenticationNotConfiguredException()
        }

        return restClientProvider.bitbucketCloudRestClient(operationDescription)
            .thenCompose { api -> api?.let { CompletableFuture.completedFuture(it) } }
    }
}

private data class CacheKey(
    val workspaceId: String,
    val repositorySlug: String,
    val ref: String,
) {
    fun toRepositoryId() = RepositoryId(workspaceId, repositorySlug)
}
