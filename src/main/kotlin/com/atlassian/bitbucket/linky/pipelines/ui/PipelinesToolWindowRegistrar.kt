package com.atlassian.bitbucket.linky.pipelines.ui

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LinkyBundle
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesDiscoveryListener
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.atlassian.bitbucket.linky.pipelines.ui.PipelinesToolWindowFactory.Companion.PIPELINES_TOOL_WINDOW_ID
import com.atlassian.bitbucket.linky.repository.traverseRepositories
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.registry.Registry
import com.intellij.openapi.wm.ToolWindowAnchor
import com.intellij.openapi.wm.ToolWindowManager

class PipelinesToolWindowRegistrar : BitbucketRepositoriesDiscoveryListener {

    override fun repositoriesDiscoveryCompleted(project: Project) {
        if (!Registry.get("bitbucket.linky.pipelines.tool.window.enabled").asBoolean()) return

        val bitbucketRepositoriesService = project.service<BitbucketRepositoriesService>()
        val enableToolWindow = project.traverseRepositories()
            .mapNotNull { bitbucketRepositoriesService.getBitbucketRepository(it) }
            .any { it is BitbucketRepository.Cloud }

        ApplicationManager.getApplication().invokeLater {
            with(ToolWindowManager.getInstance(project)) {
                val toolWindow = getToolWindow(PIPELINES_TOOL_WINDOW_ID)
                if (enableToolWindow) {
                    // Make existing tool window available, or create one if needed
                    (toolWindow ?: createPipelinesToolWindow()).isAvailable = true
                } else {
                    // Disable tool window only if it exists
                    toolWindow?.isAvailable = false
                }
            }
        }
    }

    private fun ToolWindowManager.createPipelinesToolWindow() =
        registerToolWindow(PIPELINES_TOOL_WINDOW_ID) {
            anchor = ToolWindowAnchor.BOTTOM
            canCloseContent = true
            contentFactory = PipelinesToolWindowFactory()
            icon = BitbucketLinkyIcons.Bitbucket
            shouldBeAvailable = false
            stripeTitle = LinkyBundle.getLazyMessage("toolwindow.pipelines.name")
        }
}
