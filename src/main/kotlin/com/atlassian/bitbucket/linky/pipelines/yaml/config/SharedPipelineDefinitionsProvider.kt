package com.atlassian.bitbucket.linky.pipelines.yaml.config

import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.SharedPipelinesPatterns.exportFlagPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.SharedPipelinesPatterns.pipelineDefinitionPattern
import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import com.intellij.openapi.application.runReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFileFactory
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YamlRecursivePsiElementVisitor
import java.util.concurrent.CompletableFuture

interface SharedPipelineDefinitionsProvider {
    fun getSharedPipelineDefinitions(
        workspaceId: String,
        repositorySlug: String,
        ref: String,
    ): CompletableFuture<SharedPipelineDefinitions>

    fun getSharedPipelineDefinitionsIfPresent(
        workspaceId: String,
        repositorySlug: String,
        ref: String,
    ): SharedPipelineDefinitions?
}

data class SharedPipelineDefinitions(
    val definitions: List<PipelineDefinition>,
    val exportFlagSet: Boolean,
)

data class PipelineDefinition(
    val name: String,
    val text: String,
    val startLine: Int,
    val endLine: Int,
)

class CachingSharedPipelineDefinitionsProvider(
    private val project: Project,
) : SharedPipelineDefinitionsProvider {
    private val definitionsCache: LoadingCache<String, SharedPipelineDefinitions> =
        Caffeine.newBuilder()
            .maximumSize(20)
            .weakKeys()
            .build { configuration -> parseSharedPipelineDefinitions(configuration) }

    override fun getSharedPipelineDefinitions(
        workspaceId: String,
        repositorySlug: String,
        ref: String
    ): CompletableFuture<SharedPipelineDefinitions> =
        project.service<PipelinesConfigurationProvider>()
            .getPipelinesConfiguration(workspaceId, repositorySlug, ref)
            .thenApply { configuration -> definitionsCache.get(configuration) }

    override fun getSharedPipelineDefinitionsIfPresent(
        workspaceId: String,
        repositorySlug: String,
        ref: String
    ): SharedPipelineDefinitions? =
        project.service<PipelinesConfigurationProvider>()
            .getPipelinesConfigurationIfPresent(workspaceId, repositorySlug, ref)
            ?.let { definitionsCache.get(it) }

    private fun parseSharedPipelineDefinitions(configuration: String): SharedPipelineDefinitions {
        val psiFile =
            runReadAction {
                project.service<PsiFileFactory>()
                    .createFileFromText(
                        "external-config",
                        BitbucketPipelinesYamlFileType,
                        configuration,
                    )
            }
        var export = false
        val pipelineDefinitions = mutableListOf<PipelineDefinition>()

        runReadAction {
            psiFile.accept(object : YamlRecursivePsiElementVisitor() {
                override fun visitKeyValue(keyValue: YAMLKeyValue) {
                    when {
                        exportFlagPattern.accepts(keyValue) ->
                            export =
                                runCatching { keyValue.valueText.toBoolean() }.getOrDefault(false)

                        pipelineDefinitionPattern.accepts(keyValue) ->
                            keyValue.parsePipelineDefinition(configuration)
                                ?.let(pipelineDefinitions::add)

                        else -> super.visitKeyValue(keyValue)
                    }
                }
            })
        }

        return SharedPipelineDefinitions(pipelineDefinitions, export)
    }

    private fun YAMLKeyValue.parsePipelineDefinition(configuration: String): PipelineDefinition? {
        val pipelineName = keyText
        val valueStartOffset = value?.prevSibling?.startOffset ?: return null
        val valueEndOffset = endOffset

        val definition = configuration.substring(valueStartOffset, valueEndOffset).trimIndent()

        val document = service<EditorFactory>().createDocument(configuration)
        val startLineNumber = document.getLineNumber(startOffset)
        val endLineNumber = document.getLineNumber(endOffset)

        return PipelineDefinition(pipelineName, definition, startLineNumber, endLineNumber)
    }
}
