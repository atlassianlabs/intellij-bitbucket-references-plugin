package com.atlassian.bitbucket.linky.pipelines.yaml.completion

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.actions.showNotificationForRepoAuthentication
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.pipelines.yaml.config.SharedPipelineDefinitionsProvider
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.withinYamlObject
import com.atlassian.bitbucket.linky.rest.cloud.Reference.Type.BRANCH
import com.atlassian.bitbucket.linky.rest.cloud.Reference.Type.TAG
import com.atlassian.bitbucket.linky.rest.cloud.Reference.Type.UNKNOWN
import com.intellij.codeInsight.completion.CompletionContributor
import com.intellij.codeInsight.completion.CompletionInitializationContext
import com.intellij.codeInsight.completion.CompletionParameters
import com.intellij.codeInsight.completion.CompletionProvider
import com.intellij.codeInsight.completion.CompletionResultSet
import com.intellij.codeInsight.completion.CompletionType
import com.intellij.codeInsight.lookup.LookupElement
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.icons.AllIcons
import com.intellij.openapi.application.ex.ApplicationUtil
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import com.intellij.util.ProcessingContext
import java.util.concurrent.CompletableFuture

class ImportPipelineCompletionContributor : CompletionContributor() {
    init {
        extend(
            CompletionType.BASIC,
            psiElement()
                .and(inPipelinesFilePattern)
                .and(withinYamlObject("import")),
            ImportCompletionProvider,
        )
    }

    override fun beforeCompletion(context: CompletionInitializationContext) {
        val element = context.file.findElementAt(maxOf(0, context.startOffset)) ?: return
        val caretPosition = context.startOffset
        // Look for the next colon symbol
        val colonPosition = context.editor.document.immutableCharSequence
            .subSequence(caretPosition, element.endOffset)
            .indexOf(':')
        context.replacementOffset =
            colonPosition.takeIf { it >= 0 }?.plus(caretPosition) ?: element.endOffset
    }
}

private object ImportCompletionProvider : CompletionProvider<CompletionParameters>() {
    private val log = logger()

    override fun addCompletions(
        parameters: CompletionParameters,
        context: ProcessingContext,
        result: CompletionResultSet,
    ) {
        try {
            val cloudRepo = getBitbucketRepository(parameters) ?: return
            val segment = getImportSegment(parameters) ?: return

            val prefixedResult = result.withPrefixMatcher(segment.prefix).caseInsensitive()

            val indicator = service<ProgressManager>().progressIndicator
            indicator.text = message("completion.fetching.options")

            val lookupResult = ApplicationUtil.runWithCheckCanceled(
                getOptions(cloudRepo.repository.project, cloudRepo.workspaceId, segment),
                indicator,
            )

            prefixedResult.addAllElements(lookupResult.items)
            lookupResult.hint?.let { prefixedResult.addLookupAdvertisement(it) }

            // We fetch a single page with max size of 100, assume there are more items if page is full
            if (lookupResult.items.size == 100) {
                prefixedResult.restartCompletionOnAnyPrefixChange()
            }
        } catch (e: AuthenticationNotConfiguredException) {
            parameters.editor.project?.let { showNotificationForRepoAuthentication(it) }
        }
    }

    /**
     * Returns Bitbucket Cloud repository which the pipeline configuration file belongs to.
     */
    private fun getBitbucketRepository(
        parameters: CompletionParameters,
    ): BitbucketRepository.Cloud? {
        val project = parameters.editor.project ?: return null
        val file = parameters.originalFile.virtualFile ?: return null
        val repository = project.service<VcsRepositoryManager>()
            .getRepositoryForFile(file, true)
            ?: return null
        return project.service<BitbucketRepositoriesService>()
            .getBitbucketRepository(repository)
            as? BitbucketRepository.Cloud
    }

    /**
     * Returns the import segment under the caret (repository slug, ref name or pipeline name)
     * with its start and end offsets, the prefix to the left from the caret, and the context
     * (repository slug for the ref, and slug and ref name for the pipeline name).
     */
    private fun getImportSegment(parameters: CompletionParameters): ImportSegment? {
        val caretPosition = parameters.offset
        val document = parameters.editor.document
        val elementRange = parameters.position.textRange
        parameters.position.textOffset

        // Safety net to never end up beyond the document bounds
        val elementStartOffset = maxOf(elementRange.startOffset, 0)

        if (caretPosition < elementStartOffset) {
            log.error("Caret position $caretPosition is before element offset $elementStartOffset")
            return null
        }

        val prefixText = document.getText(TextRange.create(elementStartOffset, caretPosition))

        val parts = prefixText.split(':', limit = 3)
        return when (parts.size) {
            1 -> ImportSegment.Repository(parts[0])

            2 -> parts[1]
                .takeIf { parts[0].isNotBlank() }
                ?.let { prefix -> ImportSegment.Ref(parts[0], prefix) }

            3 -> parts[2]
                .takeIf { parts[0].isNotBlank() && parts[1].isNotBlank() }
                ?.let { prefix -> ImportSegment.PipelineName(parts[0], parts[1], prefix) }

            else -> null.also {
                log.error("Unexpected ${parts.size} segments in prefix '$prefixText'")
            }
        }
    }

    private fun getOptions(
        project: Project,
        workspaceId: String,
        segment: ImportSegment,
    ): CompletableFuture<LookupResult> {
        val optionsProvider = project.service<ImportSegmentOptionsProvider>()
        return when (segment) {
            is ImportSegment.Repository ->
                optionsProvider.fetchWorkspaceRepositories(workspaceId, segment.prefix)
                    .thenApply { repos ->
                        repos.map {
                            LookupElementBuilder.create(it.slug)
                                .withLookupString(it.name)
                                .withIcon(AllIcons.Vcs.Folders)
                                .withTypeText(it.name)
                        }
                    }
                    .thenApply { LookupResult(it) }

            is ImportSegment.Ref ->
                optionsProvider.fetchRepositoryRefs(workspaceId, segment.repoSlug, segment.prefix)
                    .thenApply { refs ->
                        refs.map {
                            LookupElementBuilder.create(it.name)
                                .withIcon(
                                    when (it.type) {
                                        BRANCH -> AllIcons.Vcs.Branch
                                        TAG -> AllIcons.Gutter.ExtAnnotation
                                        UNKNOWN -> AllIcons.RunConfigurations.TestUnknown
                                    }
                                )
                        }
                    }
                    .thenApply { LookupResult(it) }

            is ImportSegment.PipelineName ->
                project.service<SharedPipelineDefinitionsProvider>()
                    .getSharedPipelineDefinitions(workspaceId, segment.repoSlug, segment.ref)
                    .thenApply { pipelineDefinitions ->
                        LookupResult(
                            pipelineDefinitions.definitions
                                .map {
                                    LookupElementBuilder.create(it.name)
                                        .withIcon(AllIcons.Toolwindows.ToolWindowTodo)
                                },
                            if (pipelineDefinitions.exportFlagSet) "Export flag not set" else null
                        )
                    }
        }
    }
}

private sealed class ImportSegment {
    abstract val prefix: String

    data class Repository(
        override val prefix: String,
    ) : ImportSegment()

    data class Ref(
        val repoSlug: String,
        override val prefix: String,
    ) : ImportSegment()

    data class PipelineName(
        val repoSlug: String,
        val ref: String,
        override val prefix: String,
    ) : ImportSegment()
}

private data class LookupResult(
    val items: List<LookupElement>,
    val hint: String? = null,
)
