package com.atlassian.bitbucket.linky.pipelines.yaml.schema

import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.intellij.openapi.application.PathManager.getConfigDir
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.jetbrains.jsonSchema.extension.JsonSchemaFileProvider
import com.jetbrains.jsonSchema.extension.JsonSchemaProviderFactory
import com.jetbrains.jsonSchema.extension.SchemaType
import java.nio.file.Path
import kotlin.io.path.exists

class PipelinesJsonSchemaProviderFactory : JsonSchemaProviderFactory {
    override fun getProviders(project: Project) = listOf(PipelinesJsonSchemaProvider)
}

const val SCHEMA_FILE_NAME = "bitbucket-pipelines.schema.json"
fun getLocalSchemaFilePath(): Path = getConfigDir().resolve("schemas").resolve(SCHEMA_FILE_NAME)

object PipelinesJsonSchemaProvider : JsonSchemaFileProvider {
    override fun getName() = "Bitbucket Pipelines configuration"

    override fun isAvailable(file: VirtualFile) =
        file.fileType == BitbucketPipelinesYamlFileType

    override fun getSchemaFile(): VirtualFile? {
        val localSchemaPath = getLocalSchemaFilePath()
        return if (localSchemaPath.exists()) {
            VfsUtil.findFile(localSchemaPath, true)
        } else {
            val resourcePath = "/schemas/$SCHEMA_FILE_NAME"
            val resource = javaClass.getResource(resourcePath) ?: throw IllegalStateException("No schema found at $resourcePath")
            VfsUtil.findFileByURL(resource)
        }
    }

    override fun getSchemaType() = SchemaType.embeddedSchema
}
