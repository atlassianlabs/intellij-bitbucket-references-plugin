package com.atlassian.bitbucket.linky.pipelines.yaml.pattern

import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns.psiElement
import org.jetbrains.yaml.psi.YAMLDocument
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLScalar

object SharedPipelinesPatterns {
    val exportFlagPattern: ElementPattern<YAMLKeyValue> =
        psiElement(YAMLKeyValue::class.java)
            .and(inPipelinesFilePattern)
            .and(yamlKeyValuePattern("export"))
            .withSuperParent(2, YAMLDocument::class.java)

    val pipelineDefinitionPattern: ElementPattern<YAMLKeyValue> =
        psiElement(YAMLKeyValue::class.java)
            .and(inPipelinesFilePattern)
            .withSuperParent(2, yamlKeyValuePattern("pipelines"))
            .withSuperParent(4, yamlKeyValuePattern("definitions"))
            .withSuperParent(6, YAMLDocument::class.java)

    val importStatementPattern: ElementPattern<YAMLScalar> =
        psiElement(YAMLScalar::class.java)
            .and(inPipelinesFilePattern)
            .withParent(yamlKeyValuePattern("import"))
}
