package com.atlassian.bitbucket.linky.pipelines.yaml.completion

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloudApi
import com.atlassian.bitbucket.linky.rest.cloud.CloudPage
import com.atlassian.bitbucket.linky.rest.cloud.Reference
import com.atlassian.bitbucket.linky.rest.cloud.Repository
import com.atlassian.bitbucket.linky.rest.cloud.RepositoryId
import com.atlassian.bitbucket.linky.rest.cloud.WorkspaceId
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import java.util.concurrent.CompletableFuture

interface ImportSegmentOptionsProvider {
    fun fetchWorkspaceRepositories(
        workspaceId: String,
        matching: String,
    ): CompletableFuture<List<Repository>>

    fun fetchRepositoryRefs(
        workspaceId: String,
        repositorySlug: String,
        matching: String,
    ): CompletableFuture<List<Reference>>
}

class AuthenticationNotConfiguredException : RuntimeException()

class DefaultImportSegmentOptionsProvider(
    private val project: Project,
) : ImportSegmentOptionsProvider {
    override fun fetchWorkspaceRepositories(
        workspaceId: String,
        matching: String,
    ): CompletableFuture<List<Repository>> =
        getClient(message("completion.fetch.repositories"))
            .thenCompose { api ->
                api.workspace(WorkspaceId(workspaceId))
                    .repositories(matching)
                    .thenApply(CloudPage<Repository>::items)
            }

    override fun fetchRepositoryRefs(
        workspaceId: String,
        repositorySlug: String,
        matching: String,
    ): CompletableFuture<List<Reference>> =
        getClient(message("completion.fetch.refs"))
            .thenCompose { api ->
                api.repository(RepositoryId(workspaceId, repositorySlug))
                    .refs()
                    .list(matching)
                    .thenApply(CloudPage<Reference>::items)
            }

    private fun getClient(operationDescription: String): CompletableFuture<BitbucketCloudApi> {
        val restClientProvider = project.service<BitbucketRestClientProvider>()
        if (!restClientProvider.isCloudAuthenticationConfigured()) {
            throw AuthenticationNotConfiguredException()
        }

        return restClientProvider.bitbucketCloudRestClient(operationDescription)
            .thenCompose { api -> api?.let { CompletableFuture.completedFuture(it) } }
    }
}
