package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.cacheDefinitionPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.serviceDefinitionPattern
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.LocalQuickFix
import com.intellij.codeInspection.ProblemDescriptor
import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElementVisitor
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.search.searches.ReferencesSearch
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YamlPsiElementVisitor

class UnusedDefinitionInspection : LocalInspectionTool() {

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitKeyValue(keyValue: YAMLKeyValue) {
            if (cacheDefinitionPattern.accepts(keyValue)) {
                handleDefinition(
                    keyValue,
                    holder,
                    { message("inspections.unused.cache.definition.problem.text", it) },
                    { removeUnusedCacheDefinitionQuickFix(it) }
                )
            } else if (serviceDefinitionPattern.accepts(keyValue)) {
                handleDefinition(
                    keyValue,
                    holder,
                    { message("inspections.unused.service.definition.problem.text", it) },
                    { removeUnusedServiceDefinitionQuickFix(it) }
                )
            }
        }
    }

    private fun handleDefinition(
        keyValue: YAMLKeyValue,
        holder: ProblemsHolder,
        errorMessageProvider: (String) -> String,
        quickFixProvider: (String) -> LocalQuickFix
    ) {
        val entityName = keyValue.keyText

        val anyReference =
            ReferencesSearch.search(
                keyValue,
                GlobalSearchScope.fileScope(keyValue.containingFile)
            ).findFirst()

        if (anyReference == null) {
            holder.registerProblem(
                keyValue,
                errorMessageProvider(entityName),
                ProblemHighlightType.LIKE_UNUSED_SYMBOL,
                quickFixProvider(entityName)
            )
        }
    }
}

private fun removeUnusedCacheDefinitionQuickFix(cacheName: String) =
    RemoveUnusedDefinitionQuickFix(
        message("inspections.unused.cache.definition.problem.fix.family.name"),
        message("inspections.unused.cache.definition.problem.fix.name", cacheName)
    )

private fun removeUnusedServiceDefinitionQuickFix(serviceName: String) =
    RemoveUnusedDefinitionQuickFix(
        message("inspections.unused.service.definition.problem.fix.family.name"),
        message("inspections.unused.service.definition.problem.fix.name", serviceName)
    )

private class RemoveUnusedDefinitionQuickFix(
    private val familyName: String,
    private val name: String
) : LocalQuickFix {
    override fun getName() = name

    override fun getFamilyName() = familyName

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        val keyValue = descriptor.psiElement as? YAMLKeyValue ?: return
        val parentMapping = keyValue.parentMapping ?: return

        parentMapping.deleteKeyValue(keyValue)
    }
}
