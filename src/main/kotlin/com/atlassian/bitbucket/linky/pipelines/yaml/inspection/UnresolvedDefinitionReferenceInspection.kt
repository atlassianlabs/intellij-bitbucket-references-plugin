package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.CacheDefinitionReference
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.DefinitionReference
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.ServiceDefinitionReference
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.YAMLScalar
import org.jetbrains.yaml.psi.YamlPsiElementVisitor

class UnresolvedDefinitionReferenceInspection : LocalInspectionTool() {
    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitScalar(scalar: YAMLScalar) {
            val reference = scalar.reference as? DefinitionReference ?: return

            if (!reference.isSoft && reference.resolve() == null) {
                val name = scalar.textValue
                val errorMessage = when (reference) {
                    is CacheDefinitionReference ->
                        message("inspections.unresolved.cache.definition.reference.problem.text", name)
                    is ServiceDefinitionReference ->
                        message("inspections.unresolved.service.definition.reference.problem.text", name)
                }

                holder.registerProblem(
                    scalar,
                    errorMessage,
                    ProblemHighlightType.LIKE_UNKNOWN_SYMBOL
                )
            }
        }
    }
}
