package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.intellij.openapi.editor.actions.EditorActionUtil
import com.intellij.openapi.fileEditor.FileEditorManager
import org.jetbrains.yaml.YAMLUtil
import org.jetbrains.yaml.psi.YAMLSequenceItem

fun YAMLSequenceItem.deleteAndPositionCaret() {
    YAMLUtil.deleteSurroundingWhitespace(this)

    delete()

    // Caret might be in the beginning of the line after removing sequence item, so send it to the end of line
    FileEditorManager.getInstance(project)
        .selectedTextEditor
        ?.let {
            EditorActionUtil.moveCaretToLineEnd(it, false)
        }
}
