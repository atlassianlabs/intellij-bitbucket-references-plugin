package com.atlassian.bitbucket.linky.pipelines.yaml.template

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.pipelinesListObjectPattern
import com.intellij.codeInsight.template.TemplateActionContext
import com.intellij.codeInsight.template.TemplateContextType
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiUtilCore
import org.jetbrains.yaml.YAMLLanguage

class PipelinesContextType : TemplateContextType("Bitbucket Pipelines") {
    override fun isInContext(context: TemplateActionContext): Boolean =
        pipelinesElement(context.file, context.startOffset) != null
}

class PipelinesListObjectContextType : TemplateContextType("Pipelines list object") {
    override fun isInContext(context: TemplateActionContext): Boolean =
        pipelinesElement(context.file, context.startOffset)
            ?.let { pipelinesListObjectPattern.accepts(it) }
            ?: false
}

private fun pipelinesElement(file: PsiFile, offset: Int): PsiElement? {
    if (inPipelinesFilePattern.accepts(file) && !isEmbeddedContent(file, offset)) {
        return file.findElementAt(offset)
    }
    return null
}

private fun isEmbeddedContent(file: PsiFile, offset: Int): Boolean {
    val languageAtOffset = PsiUtilCore.getLanguageAtOffset(file, offset)
    return !languageAtOffset.isKindOf(YAMLLanguage.INSTANCE)
}
