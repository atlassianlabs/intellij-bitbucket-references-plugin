package com.atlassian.bitbucket.linky.icons

import com.intellij.openapi.util.IconLoader.getIcon

object BitbucketLinkyIcons {

    val Bitbucket = loadIcon("/icons/bitbucket.png")

    object Actions {
        val FileCopyUrl = loadIcon("/icons/file-copy-link.png")
        val FileOpen = loadIcon("/icons/file-open.png")

        val CommitCopyUrl = loadIcon("/icons/commit-copy-link.png")
        val CommitOpen = loadIcon("/icons/commit-open.png")

        val PullRequestCopyUrl = loadIcon("/icons/pr-copy-link.png")
        val PullRequestOpen = loadIcon("/icons/pr-open.png")
        val PullRequestFind = loadIcon("/icons/pr-find.png")
        val PullRequestCreate = loadIcon("/icons/pr-create.png")

        val SnippetCreate = loadIcon("/icons/snippet-create.png")
    }

    object Settings {
        val UnknownRepository = loadIcon("/icons/repo-unsupported.png")
        val LinkableRepository = loadIcon("/icons/repo-unlinked.png")
    }

    private fun loadIcon(path: String) = getIcon(path, BitbucketLinkyIcons::class.java)
}
