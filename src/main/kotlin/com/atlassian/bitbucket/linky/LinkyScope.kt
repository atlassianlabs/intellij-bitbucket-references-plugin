package com.atlassian.bitbucket.linky

import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope

@Service(Service.Level.PROJECT)
class LinkyScope(val coroutineScope: CoroutineScope) {
    companion object {
        fun scope(project: Project) = project.service<LinkyScope>().coroutineScope
    }
}
