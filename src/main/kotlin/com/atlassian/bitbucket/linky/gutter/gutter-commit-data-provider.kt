package com.atlassian.bitbucket.linky.gutter

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.LinkyFileActionContext
import com.atlassian.bitbucket.linky.actions.linkyActionContext
import com.atlassian.bitbucket.linky.revisionHash
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.vcs.actions.ShowAnnotateOperationsPopup
import com.intellij.openapi.vcs.annotate.FileAnnotation

interface GutterCommitDataProvider {
    fun getGutterCommitData(event: AnActionEvent): GutterCommitData?
}

data class GutterCommitData(
    val ctx: LinkyFileActionContext,
    val actualLineNumber: Int? = null,
    val revision: Revision? = null
)

class DefaultGutterCommitDataProvider(private val annotation: FileAnnotation) : GutterCommitDataProvider {
    override fun getGutterCommitData(event: AnActionEvent): GutterCommitData? {
        val ctx = event.linkyActionContext()
        return if (ctx is LinkyFileActionContext) {
            getLineNumberAndRevision(event.dataContext)
                ?.let { (line, revision) -> GutterCommitData(ctx, line, revision) }
                ?: GutterCommitData(ctx)
        } else {
            null
        }
    }

    private fun getLineNumberAndRevision(dataContext: DataContext): Pair<Int, Revision>? =
        ShowAnnotateOperationsPopup
            .getAnnotationLineNumber(dataContext)
            .takeIf { it >= 0 }?.let {
                annotation.getLineRevisionNumber(it)?.revisionHash?.let { revision ->
                    Pair(
                        it,
                        revision
                    )
                }
            }
}
