package com.atlassian.bitbucket.linky.selection

import kotlin.math.max
import kotlin.math.min

data class LinesSelection(val firstLineNumber: Int, val lastLineNumber: Int) : Comparable<LinesSelection> {
    init {
        if (firstLineNumber > lastLineNumber) {
            throw IllegalArgumentException(
                "Expected first line number $firstLineNumber to be not greater than last line number $lastLineNumber"
            )
        }
    }

    override fun compareTo(other: LinesSelection): Int =
        when (firstLineNumber) {
            other.firstLineNumber -> lastLineNumber - other.lastLineNumber
            else -> firstLineNumber - other.firstLineNumber
        }
}

fun List<LinesSelection>.compileStringReference(
    prefix: CharSequence,
    selectionsDelimiter: CharSequence,
    intervalSign: CharSequence,
    suffix: CharSequence
): String? {
    val selections = mutableListOf<LinesSelection>()
    var currentSelection: LinesSelection? = null

    for (s in distinct().sorted()) {
        if (currentSelection == null) {
            currentSelection = s
        } else {
            val currentStart = currentSelection.firstLineNumber
            val currentEnd = currentSelection.lastLineNumber
            val start = s.firstLineNumber
            val end = s.lastLineNumber

            if (currentEnd >= start - 1) {
                // if adjacent or overlaps, extend current selection
                currentSelection = LinesSelection(min(currentStart, start), max(currentEnd, end))
            } else {
                // if not, push current selection to result list and update with new value
                selections.add(currentSelection)
                currentSelection = s
            }
        }
    }
    // push last current selection
    if (currentSelection != null) {
        selections.add(currentSelection)
    }

    val serialized = selections.map {
        val start = it.firstLineNumber
        val end = it.lastLineNumber
        when {
            start < end -> "$start$intervalSign$end"
            else -> "$start"
        }
    }.joinToString(selectionsDelimiter, prefix, suffix)

    return when {
        serialized.length == prefix.length + suffix.length -> null
        else -> serialized
    }
}
