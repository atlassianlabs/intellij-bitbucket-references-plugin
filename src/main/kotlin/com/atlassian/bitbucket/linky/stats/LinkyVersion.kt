package com.atlassian.bitbucket.linky.stats

import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.extensions.PluginId

object LinkyVersion {
    private const val ID: String = "com.atlassian.bitbucket.references"

    val version: String =
        PluginManagerCore
            .getPlugin(PluginId.getId(ID))
            ?.version
            ?: "unknown"
}
