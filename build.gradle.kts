import de.undercouch.gradle.tasks.download.Download
import org.jetbrains.changelog.Changelog
import org.jetbrains.changelog.markdownToHTML
import org.jetbrains.intellij.platform.gradle.TestFrameworkType

plugins {
    id("org.jetbrains.changelog") version "2.2.1"
    id("org.jetbrains.intellij.platform") version "2.0.1"
    id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    id("de.undercouch.download") version "5.6.0"
    kotlin("jvm") version "2.0.10"
}

val intellijBuild: String by project
val failOnCompilationWarnings: String by project

val releaseChannel = System.getenv("BITBUCKET_DEPLOYMENT_ENVIRONMENT")?.substringBefore('-') ?: "local"
val stableRelease = "stable".equals(releaseChannel, ignoreCase = true)
val buildNumber = System.getenv("BITBUCKET_BUILD_NUMBER") ?: "0"

group = "com.atlassian.bitbucket.pipelines"
version = "$intellijBuild.$buildNumber"

logger.lifecycle("Building Linky against IntelliJ $intellijBuild")

repositories {
    mavenCentral()
    intellijPlatform {
        defaultRepositories()
    }
}

configurations.all {
    // Dependencies provided by IntelliJ runtime
    exclude(" com.google.code.gson", "gson")
    // Junit 4 should not be used
    exclude("junit", "junit")
}

dependencies {
    intellijPlatform {
        intellijIdeaCommunity(intellijBuild)
        bundledPlugin("org.jetbrains.plugins.yaml")
        bundledPlugin("com.jetbrains.sh")
        bundledPlugin("Git4Idea")
        pluginVerifier()
        instrumentationTools()

        testFramework(TestFrameworkType.Platform)
    }

    implementation(kotlin("stdlib"))
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("stdlib-common"))

    val commonsTextVersion = "1.11.0"
    val commonmarkVersion = "0.21.0"
    val fuelVersion = "2.3.1"
    val nanoHttpdVersion = "2.3.1"

    implementation("org.apache.commons", "commons-text", commonsTextVersion)
    implementation("org.commonmark", "commonmark", commonmarkVersion)
    implementation("org.nanohttpd", "nanohttpd", nanoHttpdVersion)
    implementation("com.github.kittinunf.fuel", "fuel", fuelVersion)
    implementation("com.github.kittinunf.fuel", "fuel-gson", fuelVersion)

    val assertkVersion = "0.28.0"
    val jsonassertVersion = "1.5.1"
    val junitVersion = "5.10.2"
    val mockkVersion = "1.13.9"
    val slf4jVersion = "1.7.36"
    val wiremockVersion = "2.35.1"

    testImplementation("io.mockk", "mockk", mockkVersion)
    testImplementation("com.willowtreeapps.assertk", "assertk-jvm", assertkVersion)
    testImplementation("org.skyscreamer", "jsonassert", jsonassertVersion)
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testImplementation("org.junit.jupiter", "junit-jupiter-params", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
    testImplementation("com.github.tomakehurst", "wiremock-jre8", wiremockVersion)
    testRuntimeOnly("org.slf4j", "slf4j-simple", slf4jVersion)
}

intellijPlatform {
    pluginConfiguration {
        ideaVersion {
            untilBuild = provider { null }
        }

        val buildSuffix =
            when (releaseChannel) {
                "stable" -> ""
                "eap" -> ".eap"
                else -> ".${releaseChannel.substring(0, 1)}"
            }
        version = "${project.version}$buildSuffix"

        description =
            provider {
                projectDir
                    .resolve("README.md")
                    .readText()
                    .lines()
                    .run {
                        val start = "<!-- Plugin description -->"
                        val end = "<!-- Plugin description end -->"

                        if (!containsAll(listOf(start, end))) {
                            throw GradleException("Plugin description section not found in README.md:\n$start ... $end")
                        }
                        subList(indexOf(start) + 1, indexOf(end))
                    }.joinToString("\n")
                    .let { markdownToHTML(it) }
            }

        changeNotes.set(
            provider {
                changelog.renderItem(
                    changelog.getOrNull("${project.version}") ?: changelog.getUnreleased(),
                    Changelog.OutputType.HTML,
                )
            }
        )
    }

    buildSearchableOptions = true

    pluginVerification {
        ides {
            recommended()
        }
    }

    publishing {
        token.set(System.getenv("JB_API_TOKEN"))
        if (stableRelease.not()) {
            channels.set(listOf(releaseChannel))
        }
    }
}

changelog {
    groups.set(listOf("Added", "Changed", "Removed", "Fixed"))
}

val javaVersion = JavaVersion.VERSION_17

java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

kotlin {
    compilerOptions {
        jvmToolchain(JavaVersion.VERSION_17.majorVersion.toInt())
        allWarningsAsErrors = failOnCompilationWarnings.toBoolean()
    }
}

ktlint {
    version = "0.50.0"
}

idea {
    module {
        isDownloadSources = true
    }
}

tasks {
    register<Download>("downloadPipelinesJsonSchema") {
        val schemaFile =
            sourceSets.main
                .get()
                .resources.sourceDirectories
                .map { it.resolve("schemas/bitbucket-pipelines.schema.json") }
                .first { it.exists() }

        src("https://api.bitbucket.org/schemas/pipelines-configuration")
        dest(schemaFile)
        overwrite(true)
    }
}
