# Changelog

## Unreleased

### Added

### Changed

### Removed

### Fixed

## 2023.1.304 - 2024-10-09

### Changed

- Updated bundled Bitbucket Pipelines schema to the latest available.

### Fixed

- Fix action update thread error shown for Open selected pull request... actions

## 2023.1.301 - 2024-08-19

### Fixed

- Fix error reported on startup in IntelliJ 2024.2: use services on-demand.
- Fix missing progress indicator error reported on project open or external update.

## 2023.1.292 - 2024-06-18

### Changed

- Bundle the latest official Pipelines JSON schema, which serves a fallback for the case
  when that schema couldn't be fetched from Bitbucket.

### Removed

- Internal version of the Pipelines JSON schema. Instead, internal-only values are highlighted by means of inspection.

## 2023.1.282 - 2024-04-21

### Fixed

- Set action update thread for _Create snippet_ action

## 2023.1.281 - 2024-02-19

### Added

- Support the syntax for importing pipelines from external repositories, see
  [shared pipelines workflow configurations](https://bitbucket.org/blog/share-ci-config).
- Auto-completion for import pipeline statement segments.
- Link import pipeline statement to the imported Pipeline configuration file.

### Changed

- Use the public JSON schema for Pipelines configuration YAML file,
  automatically update it.

### Fixed

- Compatibility issues with recent IntelliJ releases (settings screens, etc.).

## 2022.1.242 - 2023-08-01

### Changed

- Update Bitbucket Pipelines internal step sizes schema

### Fixed

- Fix the build against the latest EAP release.
- Add required properties to configurables.

## 2022.1.224 - 2023-03-30

### Fixed

- Make Bitbucket Pipelines configuration file JSON schema compatible
  with other linters. The old version likely exploited some implementation
  detail of the JSON/YAML validation engine used by IntelliJ.
- Fix the error reported for long calls on EDT by Linky actions.

## 2022.1.223 - 2023-02-22

### Changed

- Update Bitbucket Pipelines JSON schema with the latest features

### Fixed

- Fix NPE when probing for Bitbucket Server using specific Git remote URLs

## 2022.1.220 - 2022-12-01

### Changed

- Configuration screens switched to the new IntelliJ UI framework.
- Simplified automatic vs specific remote URL selection.

### Fixed

- Fixed the error raised in Search Everywhere dialog in 2022.3.

## 2022.1.214 - 2022-11-14

### Changed

- Address IntelliJ Platform API deprecation notices.

### Fixed

- Fix reading resource files: remove leading slash from path.
