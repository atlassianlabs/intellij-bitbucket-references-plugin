# Bitbucket Linky plugin for IntelliJ Platform #

This is a plugin for [IntelliJ IDEA Platform](https://www.jetbrains.com/idea/plugins/).
<!-- Plugin description -->
Provides easy navigation and interaction with [Atlassian Bitbucket](https://www.atlassian.com/software/bitbucket).

**Please note that Linky is not officially supported by Atlassian.**

Linky automatically detects repositories hosted on **Bitbucket Cloud** or **Bitbucket Server** and enables
relevant actions in various places of the UI:

* **Copy link / Open in Browser.** Works for files, text selections and commits.
* **Find related Pull Requests.** Shows a list of all Pull Requests which include the selected commit.
* **Open Pull Request creation page in the Browser.** Pre-fills source and target branches.
* **Create a Snippet.** Takes selected text or files and creates a snippet in Bitbucket Cloud.

Linky also adds support for [**Bitbucket Pipelines**](https://bitbucket.org/product/features/pipelines) configuration
file `bitbucket-pipelines.yml` which includes auto-completion, navigation and validation.

### Usage stats

Bitbucket Linky collects information about your usage of the features it provides and reports this information
back to the author of the plugin via Amplitude. This information is used to get insights on how Linky is used so that
it can be improved to provide better value in the future. This information is anonymous and doesn't contain
any personal data.
<!-- Plugin description end -->

## Development

To build the plugin:

* install `gradle`
* run `gradle clean build`

To run IDEA with the plugin:

* run `gradle runIde` or use that gradle task to create a run/debug configuration in IDEA

## Publishing the plugin

In order to publish Linky you need to configure your credentials for [https://hub.jetbrains.com/](https://hub.jetbrains.com/):

```bash
gradle addCredentials --key linkyPublishUsername --value {username} -PcredentialsPassphrase={passphrase}
gradle addCredentials --key linkyPublishPassword --value {password} -PcredentialsPassphrase={passphrase}
```

This will encrypt and save provided username and password in `$HOME/.gralde/gradle.{md5OfYourPassphrase}.encrypted.properties`.

To publish the plugin:

```bash
gradle -PcredentialsPassphrase={passphrase} clean publishPlugin
```

## License and Copyright

Please see the included [license file](META-INF/license.txt) for details.

Portions of this code are derived from [Crucible4IDEA](https://github.com/ktisha/Crucible4IDEA/)
and are copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)

Huge thanks to **Brent Plump**, **Zaki Salleh**, **Piotr Wilczyński** and **Gerry Tan**!
